//
//  CheckedCollectionViewCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 20/05/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit

class CheckedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var btn_checked: UIButton!
}
