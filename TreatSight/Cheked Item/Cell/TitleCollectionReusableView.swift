//
//  TitleCollectionReusableView.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 20/05/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit

class TitleCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var lbl_header: UILabel!

}
