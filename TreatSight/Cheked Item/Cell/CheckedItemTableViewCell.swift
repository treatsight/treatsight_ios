//
//  CheckedItemTableViewCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 30/04/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit

class CheckedItemTableViewCell: UITableViewCell {

    @IBOutlet weak var img_1: UIImageView!
    @IBOutlet weak var img_2: UIImageView!
    
    @IBOutlet weak var btn_checked_2: UIButton!
    @IBOutlet weak var lbl_item2: UILabel!
    @IBOutlet weak var lbl_itemName: UILabel!
    @IBOutlet weak var btn_checked: UIButton!
    @IBOutlet weak var lbl_header: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
