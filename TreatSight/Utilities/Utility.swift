//
//  Utility.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 04/06/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit

class Utility: NSObject {

}

protocol DismissalDelegate : class
{
    func finishedShowing(viewController: UIViewController);
}

protocol Dismissable : class
{
    var dismissalDelegate : DismissalDelegate? { get set }
}

extension DismissalDelegate where Self: UIViewController
{
    func finishedShowing(viewController: UIViewController) {
        if viewController.isBeingPresented && viewController.presentingViewController == self
        {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
}
