//
//  Constant.swift
//  Music App
//
//  Created by KUMAR GAURAV on 04/04/20.
//  Copyright © 2020 Music. All rights reserved.
//

import UIKit
import Foundation


let DEVICE_TOKEN = "Device_token"

let nc = NotificationCenter.default
let defaults = UserDefaults.standard
let MY_COLLECTIONS = "my_collections_keys"
let COLLECTIONS_DATE_TIME = "collections_date_time"
let kBaseURL = "https://bharatscan.sortstring.com/api/"
let USER_ID_LOGIN = "USERID"
let MOBILE = "mobile"
let IS_LOGGED_IN = "isLoggedIn"



let USER_ID = "id"
let IS_LOGIN = "isLogin"
let FOLDER_NAME = "BharatScan"

let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

var str_UserID : String?
func getUserID()->String{
    if let str_UserID = (UserDefaults.standard.value(forKey: USER_ID_LOGIN) as? String){
    return str_UserID
    }
    else{
        return ""
    }
}


// Screen width.
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

//let themeColor = UIColor(red: 87.0/255.0, green: 182.0/255.0, blue: 195.0/255.0, alpha: 1.0)
let themeColor_blue = UIColor(red: 0/255.0, green: 174.0/255.0, blue: 239.0/255.0, alpha: 1.0)

func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}



extension UIView{
    func addGradient(color1 : UIColor , color2 : UIColor){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
//    func addGradient_withBlue(){
//        let gradient = CAGradientLayer()
//        gradient.frame = self.bounds
//        gradient.colors = [themeColor.cgColor, UIColor(red: 238/255.0, green: 63/255.0, blue: 119/255.0, alpha: 1.0).cgColor]
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 1)
//        self.layer.insertSublayer(gradient, at: 0)
//    }
    
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 4.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 4.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func MyToast(){
        let label = UILabel(frame: CGRect(x:0, y:self.frame.size.height / 2, width:220, height:40))
        label.backgroundColor = UIColor.red
        label.center = self.center
        label.textAlignment = NSTextAlignment.center
        label.clipsToBounds  = true
        label.layer.cornerRadius = 10
        label.textColor = UIColor.white
        self.addSubview(label)
        label.fadeIn(completion: {
            (finished: Bool) -> Void in
            label.text = "Network Unavailble !"
            label.fadeOut()
        })
    }
    
    func addShadow(color:UIColor){
        layer.shadowRadius = 2.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 0, height:0)//CGSize.zero
    }
    func addSideShadow(color:UIColor){
        layer.shadowRadius = 2.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 1.5, height: 1.5)//CGSize.zero
    }
    func addShadowWithExtraWidth(color:UIColor){
        layer.shadowRadius = 4.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 3, height:3)//CGSize.zero
    }
    
}






// MARK: - Method to convert JSON String into Dictionary
func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
//            print(error.localizedDescription)
        }
    }
    return nil
}


extension String{
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}




func get_user_id()-> String{
    return defaults.value(forKey: USER_ID) as! String
}
extension UIView {
    func roundCorners_AtBottom( radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

//            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            let corners = CACornerMask()
            var cornerMask = UIRectCorner()
//                 if(corners.contains(.layerMinXMinYCorner)){
//                     cornerMask.insert(.topLeft)
//                 }
//                 if(corners.contains(.layerMaxXMinYCorner)){
//                     cornerMask.insert(.topRight)
//                 }
                 if(corners.contains(.layerMinXMaxYCorner)){
                     cornerMask.insert(.bottomLeft)
                 }
                 if(corners.contains(.layerMaxXMaxYCorner)){
                     cornerMask.insert(.bottomRight)
                 }
//                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

                 let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
                 let mask = CAShapeLayer()
                 mask.path = path.cgPath
                 self.layer.mask = mask
            
        }
    }
    
        func roundCorners_AtTopRight( radius: CGFloat) {
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            if #available(iOS 11.0, *) {
                self.layer.maskedCorners = [.layerMaxXMinYCorner]

    //            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            } else {
                let corners = CACornerMask()
                var cornerMask = UIRectCorner()
    //                 if(corners.contains(.layerMinXMinYCorner)){
                         cornerMask.insert(.topLeft)
    //                 }
    //                 if(corners.contains(.layerMaxXMinYCorner)){
    //                     cornerMask.insert(.topRight)
    //                 }
                     if(corners.contains(.layerMinXMaxYCorner)){
//                         cornerMask.insert(.bottomLeft)
                     }
                     if(corners.contains(.layerMaxXMaxYCorner)){
//                         cornerMask.insert(.bottomRight)
                     }
    //                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

                     let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
                     let mask = CAShapeLayer()
                     mask.path = path.cgPath
                     self.layer.mask = mask
                
            }
        }
}

extension UIImage{
    func resize(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}



//FUNCTION FOR GET DIRECTORY PATH

func getDirectoryPath() -> NSURL
{
  // path is main document directory path
      
    let documentDirectoryPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
    let pathWithFolderName = documentDirectoryPath.appendingPathComponent(FOLDER_NAME)
    let url = NSURL(string: pathWithFolderName) // convert path in url
      print(url)
    return url!
}


// Function For Create Folder In Document Directory
  
func CreateFolderInDocumentDirectory()
{
  let fileManager = FileManager.default
  let PathWithFolderName = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(FOLDER_NAME)
  
  print("Document Directory Folder Path :- ",PathWithFolderName)
      
  if !fileManager.fileExists(atPath: PathWithFolderName)
  {
      try! fileManager.createDirectory(atPath: PathWithFolderName, withIntermediateDirectories: true, attributes: nil)
  }
}


 

//    func getImageFromDocumentDirectory(file_name : String)->UIImage
//    {
//        let fileManager = FileManager.default
//        var tempImage = UIImage()
//
//        let imagePath = (getDirectoryPath() as NSURL).appendingPathComponent(file_name + ".jpeg") // here assigned img name who assigned to img when saved in document directory. Here I Assigned Image Name "MyImage.png"
//
//        let urlString: String = imagePath!.absoluteString
//
//        if fileManager.fileExists(atPath: urlString)
//        {
//            let GetImageFromDirectory = UIImage(contentsOfFile: urlString) // get this image from Document Directory And Use This Image In Show In Imageview
//            tempImage = GetImageFromDirectory!
//        }
////        else
////        {
////            print("No Image Found")
////        }
//        return tempImage
//    }
