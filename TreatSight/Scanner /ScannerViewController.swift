//
//  ScannerViewController.swift
//  TreatSight
//
//  Created by on 26/04/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import iOSDropDown
import NVActivityIndicatorView
import MBProgressHUD
import SVProgressHUD

let screenRect = UIScreen.main.bounds
//let screenWidth = screenRect.size.width
//let screenHeight = screenRect.size.height
class ScannerViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate,UIScrollViewDelegate,DismissalDelegate {
    
    
// MARK: - IBOutlet
// MARK: -
    
    
    @IBOutlet weak var drop: DropDown!
    @IBOutlet weak var scanImage: UIImageView!
    
    @IBOutlet weak var cons_DropDownH_5: NSLayoutConstraint!
    @IBOutlet weak var cons_DropDownH_4: NSLayoutConstraint!
    @IBOutlet weak var cons_DropDownH_3: NSLayoutConstraint!
    @IBOutlet weak var cons_DropDownH_2: NSLayoutConstraint!
    @IBOutlet weak var cons_DropDownH_1: NSLayoutConstraint!
    
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    @IBOutlet weak var consDropDownHeight: NSLayoutConstraint!
    @IBOutlet weak var consMidHeigt: NSLayoutConstraint!
    @IBOutlet weak var cons_ScannerHeight: NSLayoutConstraint!
    @IBOutlet weak var view_mid: UIView!
    @IBOutlet weak var view_dropDown: UIView!
    @IBOutlet weak var scrollViewScanner: UIScrollView!
    
    @IBOutlet weak var dropDown1: DropDown!
    @IBOutlet weak var dropDown2: DropDown! 
    @IBOutlet weak var dropDown3: DropDown!
    @IBOutlet weak var dropDown4: DropDown!
    @IBOutlet weak var dropDown5: DropDown!
    
    
    private var lastContentOffset: CGFloat = 0
    @IBOutlet weak var scannerView: UIView!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    @IBOutlet var user_Id: String!
    @IBOutlet var client_Id: String!
    
    var machines_1 =  Dictionary<String, String>()
    var machines_2 =  Dictionary<String, String>()
    var machines_3 =  Dictionary<String, String>()
    var machines_4 =  Dictionary<String, String>()
    var machines_5 =  Dictionary<String, String>()
    
    var machines =  Dictionary<String, String>()

    var client_id: String = ""
    var area_id: String = ""
    var user_id: String = ""
    var header: String = ""


    var lease_id: String = ""
    var location_id: String = ""
    var treatsite_id: String = ""
    var option = [String].self
    
    var option_dropDown1:String = ""
    var option_dropDown2:String = ""
    var option_dropDown3:String = ""
    var option_dropDown4:String = ""
    var option_dropDown5:String = ""
    
    var previousHeight_Scanner : CGFloat = 0.0
    let previousHeight_MidView : CGFloat = 0.0
    var previousHeight_DropDown : CGFloat = 0.0
    var toggle_down : Bool =  false
    var toggle_up : Bool =  false
    
    var actualHeight_DropDown_1 : CGFloat = 0.0
    var actualHeight_DropDown_2 : CGFloat = 0.0
    var actualHeight_DropDown_3 : CGFloat = 0.0
    var actualHeight_DropDown_4 : CGFloat = 0.0
    var actualHeight_DropDown_5 : CGFloat = 0.0
    
    var progressHUD = MBProgressHUD()
    
    
    // MARK: - IBAction
    // MARK: -

    @IBAction func goBtnClicked(_ sender: Any) {
     self.goToCheckedClass()
        }
    
    @IBAction func btn_LogoutAction(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            UserDefaults.standard.removeObject(forKey: "user_name")
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.removeObject(forKey: "isRemembered")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Methods for Initialization
    // MARK: -

    
   // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        print("Hello World")
        
        dropDown5.resignFirstResponder()
        dropDown4.resignFirstResponder()
        dropDown1.resignFirstResponder()
        dropDown2.resignFirstResponder()
        dropDown3.resignFirstResponder()
        

    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            print(keyboardSize)

            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        //if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                //self.view.frame.origin.y += keyboardSize.height
                self.view.frame.origin.y = 0
            }
        //}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ScannerViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector:#selector(ScannerViewController.keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil);
        
        if user_Id == nil && client_Id == nil{
            user_Id = UserDefaults.standard.value(forKey: "user_id") as? String
            client_Id = UserDefaults.standard.value(forKey: "client_id") as? String
            client_Id = UserDefaults.standard.value(forKey: "userName") as? String


        }
        self.user_id = user_Id
        self.client_id = client_Id
        self.initializesAllDropDown()
        // Do any additional setup after loading the view.
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr ,.code128]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.scannerView.frame.height)
        previewLayer.videoGravity = .resizeAspectFill
        let myLayer = CALayer()
        let overlayImage = UIImage(named: "overlayScan")?.cgImage
        myLayer.contents = overlayImage
        
        myLayer.position = CGPoint(x: 200, y: 400)
        myLayer.frame = previewLayer.frame
        previewLayer.addSublayer(myLayer)
        scannerView.layer.addSublayer(previewLayer)
        captureSession.startRunning()

    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        print(screenHeight)
        if screenHeight == 568.0{
            print("inside ..")
            
           scrollViewScanner.contentSize = CGSize(width: 0, height: self.view.frame.height + 80 )
        }

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        previousHeight_DropDown = view_dropDown.frame.height
        previousHeight_Scanner = scannerView.frame.height
        
        actualHeight_DropDown_1 = dropDown1.frame.height
        actualHeight_DropDown_2 = dropDown2.frame.height
        actualHeight_DropDown_3 = dropDown3.frame.height
        actualHeight_DropDown_4 = dropDown4.frame.height
        actualHeight_DropDown_5 = dropDown5.frame.height
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    
 
    
    
    func initializesAllDropDown(){
        
        
        dropDown1.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.option_dropDown1 = strongSelf.machines_1[string]!
            self!.area_id = strongSelf.machines_1[string]!


            self?.callSecodDropDown()
        }
        
        dropDown2.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.option_dropDown2 = strongSelf.machines_2[string]!
            self!.lease_id = strongSelf.machines_2[string]!

            self?.callThirdDropDown()
        }
        
        dropDown3.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.option_dropDown3 = strongSelf.machines_3[string]!
            strongSelf.location_id = strongSelf.machines_3[string]!
            self?.callFourthDropDown()
            
        }
        dropDown4.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.option_dropDown4 = strongSelf.machines_4[string]!
            strongSelf.treatsite_id =
                strongSelf.machines_4[string]!
            self?.header = string
            self?.callFifthDropDown()
            
        }
        dropDown5.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            //let checkval = strongSelf.machines_5[string]!
            //if(checkval != ""){
                strongSelf.option_dropDown5 = strongSelf.machines_5[string]!
                //self?.header = string
            //}
            
        }
        
        dropDown5.isUserInteractionEnabled = true
        // --------------- Drop Down for Get Area -----------------

        progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        
//        dropDown1.delegate = self
        dropDown1.selectedRowColor = themeColor()
            Alamofire.request("\(baseURL)SetFilter?&client_id=\(self.client_id)").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    if let aux:[String : Any] = json["data"] as? [String: Any]{
                    let machines = aux["areas"] as! [[String: String]]
                    var machineResults: [String] = []
                    for machine in machines {
                        machineResults.append(machine["area_name"]!)
                        strongSelf.machines_1[machine["area_name"]!] = machine["area_id"]
                    }
                        if machines.count > 0{
                    
                            let swiftArray = machineResults as AnyObject as! [String]
                            let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                            
                            let arrNewMutable = NSMutableArray(array: sortedArray)
                            strongSelf.dropDown1.optionArray = arrNewMutable as! [String]
                            
                    strongSelf.dropDown1.selectedIndex = 0
                    self!.area_id = machines[0]["area_id"]!
                    strongSelf.option_dropDown1 = machines[0]["area_id"]!
                    print("area id %d",self!.area_id)
                    strongSelf.dropDown1.text = machineResults[0]
                    self?.callSecodDropDown()
                        }
                        else{
                            DispatchQueue.global().async() {
                                // ...Run some task in the background here...
                                DispatchQueue.main.async() {
                                    //credential
                                    self!.progressHUD.hide(animated: true)
                                    let alert = UIAlertController(title: "Error", message: "Login with right credentials.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                                        self?.dismiss(animated: true, completion: nil)
                                        
                                    }))
                                    //strongSelf.present(alert, animated: true, completion: nil)
                                    self!.present(alert, animated: true, completion: nil)
                                }
                            }//End of Dispatch
                            
                    }//else
                }
            }
        }
    }
    }
    
    func callSecodDropDown(){
      
        // --------------- Drop Down for Get Lease -----------------
//        dropDown2.delegate = self
        dropDown2.selectedRowColor = themeColor()

        Alamofire.request("\(baseURL)SetFilter?client_id=\(self.client_id)&area_id=\(self.area_id)").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    if let aux:[String : Any] = json["data"] as? [String: Any]{
                    let machines = aux["leases"] as! [[String: String]]
                    var machineResults: [String] = []
                    for machine in machines {
                        machineResults.append(machine["lease_name"]!)
                        strongSelf.machines_2[machine["lease_name"]!] = machine["lease_id"]
                    }
                    
                        let swiftArray = machineResults as AnyObject as! [String]
                        let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                        
                        let arrNewMutable = NSMutableArray(array: sortedArray)
                        strongSelf.dropDown2.optionArray = arrNewMutable as! [String]
                        
                    strongSelf.dropDown2.selectedIndex = 0
                    strongSelf.dropDown2.text = machineResults[0]
                    strongSelf.option_dropDown2 = machines[0]["lease_id"]!
                    self!.lease_id = machines[0]["lease_id"]!
                    self?.callThirdDropDown()
                    }
                }
            }
        }
    }
    
    func callThirdDropDown(){
        
        // --------------- Drop Down for Get LOCATION -----------------
//        dropDown3.delegate = self
        self.dropDown3.selectedRowColor = themeColor()

        Alamofire.request("\(baseURL)SetFilter?client_id=\(self.client_id)&lease_id=\(self.lease_id)").responseJSON {[weak self] response in
            
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    if let aux:[String : Any] = json["data"] as? [String: Any]{
                    let machines = aux["locations"] as! [[String: String]]
                    var machineResults: [String] = []
                    for machine in machines {
                        machineResults.append(machine["location_name"]!)
                        strongSelf.machines_3[machine["location_name"]!] = machine["location_id"]
                    }
                    
                        let swiftArray = machineResults as AnyObject as! [String]
                        let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                        let arrNewMutable = NSMutableArray(array: sortedArray)
                        strongSelf.dropDown3.optionArray = arrNewMutable as! [String]
                        
                    strongSelf.dropDown3.selectedIndex = 0
                        strongSelf.dropDown3.text = (arrNewMutable[0] as! String)
                    self!.option_dropDown3 = machines[0]["location_id"]!
                    strongSelf.location_id = machines[0]["location_id"]!
                    self?.callFourthDropDown()
                }
                }
            }
        }
    }

    
    func callFourthDropDown(){
        
        // --------------- Drop Down for Get TREATSITE -----------------
        dropDown4.selectedRowColor = themeColor()
//        dropDown4.delegate = self
        Alamofire.request("\(baseURL)SetFilter?client_id=\(self.client_id)&location_id=\(self.location_id)").responseJSON {[weak self] response in
            
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    if let aux:[String : Any] = json["data"] as? [String: Any]{
                    let machines = aux["treatsites"] as! [[String: String]]
                    var machineResults: [String] = []
                        
                    for machine in machines {
                        machineResults.append(machine["treatsite_name"]!)
                        strongSelf.machines_4[machine["treatsite_name"]!] = machine["treatsite_id"]
                    }
                        let swiftArray = machineResults as AnyObject as! [String]
                        let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                        
                        let arrNewMutable = NSMutableArray(array: sortedArray)
                        
                    strongSelf.dropDown4.optionArray = arrNewMutable as! [String]
                    strongSelf.dropDown4.selectedIndex = 0
                    strongSelf.treatsite_id = machines[0]["treatsite_id"]!
                    
                    strongSelf.dropDown4.text = (arrNewMutable[0] as! String)
                    strongSelf.option_dropDown4 = machines[0]["treatsite_id"]!
                        
                       // print(strongSelf.option_dropDown4)
                       // print("============== st1")
                    self?.header = machines[0]["treatsite_name"]!
                        
                        self?.callFifthDropDown()
                        
                       /* DispatchQueue.global().async() {
                            // ...Run some task in the background here...
                            DispatchQueue.main.async() {
                                self!.progressHUD.hide(animated: true)
                            }
                        }//End of Dispatch
                        */
                    }
                }
            }
        }
        
    }


        func callFifthDropDown(){
            
            // --------------- Drop Down for Get TREATSITE -----------------
            dropDown5.selectedRowColor = themeColor()
    //        dropDown5.delegate = self
           let fURL = "\(baseURL)SetFilter?client_id=\(self.client_id)&treatsight_id=\(self.treatsite_id)"
            
            print(fURL)
            
            Alamofire.request(fURL).responseJSON {[weak self] response in
                
                if let json = response.result.value as? [String: Any]{
                    if (json["response"] as! NSString).intValue == 1{
                        guard let strongSelf = self else {return}
                        if let aux:[String : Any] = json["data"] as? [String: Any]{
                            
                            print(aux)
                            
                        let machines = aux["treatpoints"] as! [[String: String]]
                        var machineResults: [String] = []
                        
                            if(!machines.isEmpty){
                                machineResults.append("Setect Treatpoint")
                                strongSelf.machines_5["Setect Treatpoint"] = ""
                        for machine in machines {
                            machineResults.append(machine["treatpoint_name"]!)
                            strongSelf.machines_5[machine["treatpoint_name"]!] = machine["treatpoint_id"]
                        }
                               // print(strongSelf.machines_5)
                                
                            let swiftArray = machineResults as AnyObject as! [String]
                            let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                            
                        let arrNewMutable = NSMutableArray(array: sortedArray)
                            strongSelf.dropDown5.optionArray = arrNewMutable as! [String]
                            strongSelf.dropDown5.selectedIndex = 0
                        
                            strongSelf.treatsite_id = machines[0]["treatpoint_id"]!
                            
                            strongSelf.dropDown5.text = (arrNewMutable[0] as! String)
                            //strongSelf.dropDown5.text = "Select Treatpoint"
                            //strongSelf.option_dropDown5 = ""
                        
                            // self?.header = machines[0]["treatpoint_name"]!
                            
                            DispatchQueue.global().async() {
                                // ...Run some task in the background here...
                                DispatchQueue.main.async() {
                                    self!.progressHUD.hide(animated: true)
                                }
                            }//End of Dispatch
                            }else{
                                
                                    let swiftArray = machineResults as AnyObject as! [String]
                                    let sortedArray = swiftArray.sorted{$0.localizedStandardCompare($1) == .orderedAscending}
                                    
                                let arrNewMutable = NSMutableArray(array: sortedArray)
                                    strongSelf.dropDown5.optionArray = arrNewMutable as! [String]
                                
                                strongSelf.dropDown5.text = "Not Available"
                                
                                strongSelf.option_dropDown5 = ""
                                
                            }
                        }
                    }
                }
            }
            
        }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    

 
    
    // MARK: - Methods for Screen Settings
    // MARK: -
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else {
                return
            }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(client_id: self.client_Id, user_id: self.user_Id, code: stringValue)
            print(stringValue)

        }
        else{
            self.captureSession!.startRunning()

        }
        
    }
    
    
    func found(client_id: String, user_id: String, code: String) {
       let ResUrl = "\(baseURL)Machine?user_id=\(user_id)&client_id=\(client_id)&barcode=\(code)"
        
        Alamofire.request(ResUrl).responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [String: Any]
                    let machine = aux["machines"] as! [String: String]
                    
                    if let tp = aux["treatpoints"] as? [String: String] {
                        strongSelf.option_dropDown5 = tp["tp_id"]!
                    }
                                        
                    var machineResults: [String] = []
                    machineResults.append(machine["machine_name"]!)
                    strongSelf.machines[machine["machine_name"]!] = machine["machine_id"]
                    strongSelf.option_dropDown4 = machine["machine_id"]!
                    
                    
                    self!.header = machine["machine_name"] ?? "";
                    self?.goToCheckedClass()
                }else{
                    self!.showAlert()
                }
            }else{
                self!.showAlert()
            }
        }
        
    
    
    }

    
    func showAlert(){
        let alertController = UIAlertController(title: "Alert !", message: "Record(s) does not available.", preferredStyle: UIAlertController.Style.alert)
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.captureSession!.startRunning()
            
        }
        // Add the actions
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func goToCheckedClass(){
        let vc = storyboard!.instantiateViewController(withIdentifier: "CheckedItemVC") as! CheckedItemVC
        vc.user_Id = self.user_id
        vc.client_Id = self.client_id
        vc.option   =   self.option_dropDown4
        vc.treatpoint = self.option_dropDown5
        vc.header = self.header
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_GotoHomeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
  
   
}

extension ScannerViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
