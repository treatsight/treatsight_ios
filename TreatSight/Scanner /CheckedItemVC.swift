//
//  CheckedItemVC.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 30/04/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
var counterOfViewController:Int = 0
class CheckedItemVC: UIViewController{
    
    var arr_checkedItem: NSArray? = []
    var arr_title: NSArray? = []
    
    @IBOutlet var user_Id: String!
    @IBOutlet var client_Id: String!
    var machines =  Dictionary<String, String>()
    var client_id: String = ""
    var user_id: String = ""
    var option: String = ""
    var treatpoint: String = ""
    var header: String = ""

    
    var arrcheckedM : NSMutableArray = []
    
    
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var colView_Checked: UICollectionView!
    
    @IBOutlet weak var btn_go: UIButton!
    
    
    @IBAction func logoutBtnClicked(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func homeBtnClicked(_ sender: Any) {
        
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    @IBAction func goBtnClicked(_ sender: Any) {
        var arrViewM : NSMutableArray? = []
        arrViewM = NSMutableArray()
//        print(arrViewM as Any)
        
        for i in (0 ... arrcheckedM.count-1)
        {
            let str_checked = arrcheckedM[i] as! String
            if str_checked == "YES" {
                if(self.treatpoint == ""){
                    if i == 0{
                        arrViewM?.add("form_5")
                    }else if i == 1{
                        arrViewM?.add("form_6")
                    }else if i == 2{
                        arrViewM?.add("form_7")
                    }else if i == 3{
                        arrViewM?.add("form_8")
                    }else if i == 4{
                        arrViewM?.add("form_10")
                     }
                }else{
                    
                    if i == 0{
                        arrViewM?.add("form_1")
                    }else if i == 1{
                        arrViewM?.add("form_2")
                    }else if i == 2{
                        arrViewM?.add("form_3")
                    }else if i == 3{
                        arrViewM?.add("form_4")
                    }else if i == 4{
                        arrViewM?.add("form_5")
                    }else if i == 5{
                        arrViewM?.add("form_6")
                    }else if i == 6{
                        arrViewM?.add("form_7")
                    }else if i == 7{
                        arrViewM?.add("form_8")
                    }else if i == 8{
                        arrViewM?.add("form_9")
                    }
                    else if i == 9{
                        arrViewM?.add("form_10")
                    }
                }
                
            }
//            print(arrViewM as Any)
        }
        
//        print(arrViewM!)
        if arrViewM!.count > 0{
            
            counterOfViewController = counterOfViewController + 1
            //        arrViewM?.sort(using: ascen)
            var swiftArray = arrViewM as AnyObject as! [String]
//            let sortedArray = swiftArray.sorted { ($0 as AnyObject).localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            swiftArray.sort {$0.localizedStandardCompare($1) == .orderedAscending}
            let arrNewMutable = NSMutableArray(array: swiftArray)
//            let arrNewMutable = NSMutableArray(array: swiftArray)

            
            let val = arrNewMutable[0] as! String
            
            switch val {
            case "form_1":
                let vc = CTMFormViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_2":
                let vc = BatchTruckViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_3":
                let vc = MonitoringPlanViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_4":
                let vc = AnalysisViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_5":
                let vc = WellFailureViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_6":
                let vc = SamplingViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_7":
                let vc = WellTestViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_8":
                let vc = DaysOnViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_9":
                let vc = KnowledgeViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable )
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
            case "form_10":
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                vc.client_id = self.client_id
                vc.user_id = self.user_id
                vc.arr_ViewController = arrNewMutable
//
//                    KnowledgeViewController(self.option, self.treatpoint, self.client_id, self.user_id,arrViewM: arrNewMutable )
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                break
                
            default:
                break
            }
        }
            //When No item selected
        else {
            let alert = UIAlertController(title: "Error", message: "No Item Selected", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)        }
    }
    
    
    
    
    @IBOutlet weak var tblView_CheckedItem: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if header != ""{
        lbl_header.text = header
        }
        self.user_id = user_Id
        self.client_id = client_Id
        
        counterOfViewController = 0
        
        if(self.treatpoint == ""){
            arr_checkedItem = [
                ["header":"Downhole",
                 "item":[
                        "Well Failure",
                        "Sampling"
                        ]
                ],
                ["header":"Production",
                 "item":[
                        "Well Test",
                        "Well Days On"
                        ]
                ],
                ["header":"Knowledge",
                 "item":[
                    "Surveillancer"
                    ]
                ]
            ]
            
            arr_title = ["Downhole","Production"]
        }else{
            arr_checkedItem = [
                ["header":"Chemical Management",
                 "item":[
                        "Continous Treatement",
                        "Batch/ Truck Management"
                        ]
                ],
                ["header":"Monitoring Analytical",
                 "item":[
                        "Monitoring Plan",
                        "Analysis"
                        ]
                ],
                ["header":"Downhole",
                 "item":[
                        "Well Failure",
                        "Sampling"
                        ]
                ],
                ["header":"Production",
                 "item":[
                        "Well Test",
                        "Well Days On"
                        ]
                ],
                ["header":"Knowledge",
                 "item":["Knowledge Point","Surveillance"]
                ]
            ]

            arr_title = ["Chemical Management","Monitoring Analytical","Downhole","Production","Knowledge"]
        
        }
        
        // Do any additional setup after loading the view.
        self.setValueForCheckedItem()
    }
    
    func setValueForCheckedItem(){
        arrcheckedM = NSMutableArray()
        for _ in (0 ... 9)
        {
            arrcheckedM.add("NO")
        }
    }
}


extension UINavigationController {
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}

extension CheckedItemVC : UICollectionViewDelegate,UICollectionViewDataSource{
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let dic = arr_checkedItem?[section] as! NSDictionary
        let arr = dic.value(forKey: "item") as! NSArray
       return arr.count
    }
    
    
    
    // make a cell for each cell index path
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checked", for: indexPath as IndexPath) as! CheckedCollectionViewCell
        
        cell.btn_checked.tag = indexPath.item
        cell.btn_checked.addTarget(self, action: #selector(self.checkedAction(_:)), for: UIControl.Event.touchUpInside)
        let dic = arr_checkedItem?[indexPath.section] as! NSDictionary
        let arr = dic.value(forKey: "item") as! NSArray
        cell.lbl_itemName.text = arr[indexPath.item] as? String
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    
    
    // MARK: - Add Targets
    // MARK: -
    @objc func checkedAction(_ sender: UIButton) {
//        print(arrcheckedM)
        let index:Int
        
        if let button = sender as? UIButton {
            let point: CGPoint = button.convert(.zero, to: colView_Checked)
            if let indexPath = colView_Checked!.indexPathForItem(at: point) {
                
                let cell = colView_Checked!.cellForItem(at: indexPath) as! CheckedCollectionViewCell
                //                cell.backgroundColor = UIColor.blue
                
                index = (indexPath.section * 2) + indexPath.row
                
                let str_checked = arrcheckedM[index] as! String

                if str_checked == "NO" {
                    arrcheckedM.replaceObject(at: index as Int , with: "YES")
                    //            arrcheckedM.insert("YES", at: index as Int )
                    let image: UIImage = UIImage(named: "checked_blue2")!
                    cell.btn_checked .setImage(image, for: UIControl.State.normal)
                }
                else{
                    arrcheckedM.replaceObject(at: index as Int , with: "NO")
                    
                    let image: UIImage = UIImage(named: "uncheked_blue")!
                    cell.btn_checked .setImage(image, for: UIControl.State.normal)
                }
//                print(arrcheckedM)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var headerView: TitleCollectionReusableView?
        if kind == UICollectionView.elementKindSectionHeader {
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "title", for: indexPath) as? TitleCollectionReusableView
            let dic = arr_checkedItem?[indexPath.section] as! NSDictionary
            let strVal:String = dic.value(forKey: "header") as! String
            headerView!.lbl_header.text = strVal
            
        }
        return headerView!
    }
    
    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50) //add your height here
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return (arr_checkedItem?.count)!
    }
    
}


extension CheckedItemVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width/2)-5, height: (self.view.frame.width/3))
    }
}
