//
//  DashboardViewController.swift
//  TreatSight
//
//  Created by on 26/04/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import Alamofire

class DashboardViewController: UIViewController,DismissalDelegate {

    @IBOutlet weak var lbl_clientName: UILabel!
    @IBOutlet weak var btn_letsStart: UIButton!
    @IBOutlet var userId: String!
    @IBOutlet var clientId: String!
    
    var user_name = ""
    var isLoginScreen = "NO"

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    func setUserNameWithAttributedString(){
        print(user_name)
        if user_name == ""{
            user_name = (UserDefaults.standard.value(forKey: "userName") as? String)!
            
        }
        
        btn_letsStart.layer.cornerRadius = btn_letsStart.frame.width / 2
        btn_letsStart.layer.borderWidth = 6
        //        btn_letsStart.layer.borderColor = UIColor.cyan.cgColor
        btn_letsStart.layer.borderColor = UIColor(red: 59.0/255.0, green: 150.0/255.0, blue: 252.0/255.0, alpha: 1).cgColor
        
        counterOfViewController = 0
        
        let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: themeColor()]
        
        let firstString = NSMutableAttributedString(string: "Welcome, ", attributes: firstAttributes)
        let secondString = NSAttributedString(string: user_name, attributes: secondAttributes)
        
        firstString.append(secondString)
        //--------------------------
        //        let myString = "Welcome, " + myAttrString
        lbl_clientName.attributedText = firstString
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if (userId == "" || userId == nil){
            userId = UserDefaults.standard.value(forKey: "user_id") as? String

        }
        if (clientId == "" || clientId == nil){
            clientId = UserDefaults.standard.value(forKey: "client_id") as? String
        }
        
        setUserNameWithAttributedString()
    }
    

    @IBAction func btn_LetsStartAction(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "ScannerViewController") as! ScannerViewController
        vc.user_Id = userId
        vc.client_Id = clientId

        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true, completion: nil)
        

        
    }
    
    @IBAction func btn_LogoutAction(_ sender: Any) {
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            UserDefaults.standard.removeObject(forKey: "user_name")
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.removeObject(forKey: "isRemembered")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController 
            
            if self.isLoginScreen == "YES"{
                print(self.isLoginScreen)
                self.dismiss(animated: true, completion:nil)
                
            }
            else{
                print(self.isLoginScreen)
                loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

                self.present(loginVC, animated:true, completion:nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }

        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
 }


