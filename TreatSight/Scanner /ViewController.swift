//
//  ViewController.swift
//  TreatSight
//
//  Created by on 26/04/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire


func themeColor()->UIColor{
    let themeColor : UIColor = UIColor(red:0.12, green:0.56, blue:1.00, alpha:1.0)
    return themeColor
}

enum VendingMachineError: Error {
    case invalidSelection
    case insufficientFunds(coinsNeeded: Int)
    case outOfStock
}


class ViewController: UIViewController {
    
    @IBOutlet weak var btnRememberPassword: UIButton!
    
    @IBAction func rememberPasswordAction(_ sender: Any) {
        
        switchValue = !switchValue

        if switchValue {
            let image: UIImage = UIImage(named: "checked_blue1")!
            self.btnRememberPassword.setImage(image, for: UIControl.State.normal)
            
        }else{
            
            let image: UIImage = UIImage(named: "uncheked_blue")!
            self.btnRememberPassword .setImage(image, for: UIControl.State.normal)
        }
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
    }
    
    @IBOutlet weak var TextF_Password: UITextField!
    @IBOutlet weak var TextF_UserName: UITextField!
    @IBOutlet weak var spinner: NVActivityIndicatorView!
    
    @IBOutlet weak var rememberSwitch: UISwitch!
    var user: String = ""
    var pass:String = ""
    var switchValue: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        TextF_UserName.inputAccessoryView = nil
        TextF_Password.inputAccessoryView = nil
    
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(ViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        TextF_UserName.addTarget(self, action: Selector(("userDidChange:")), for: UIControl.Event.editingChanged)
        TextF_UserName.inputAccessoryView = getToolbar()
        TextF_UserName.becomeFirstResponder()
        TextF_Password.addTarget(self, action: Selector(("passDidChange:")), for: UIControl.Event.editingChanged)
        TextF_Password.inputAccessoryView = getToolbar()
 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.spinner.stopAnimating()

        let vc = storyboard!.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        
        if (UserDefaults.standard.value(forKey: "isRemembered") != nil) {
            
            let user_id = UserDefaults.standard.value(forKey: "user_id") as! String
            let client_id = UserDefaults.standard.value(forKey: "client_id") as! String
            let username = UserDefaults.standard.value(forKey: "userName") as! String

            vc.userId = user_id
            vc.clientId = client_id
            vc.user_name = username
            vc.isLoginScreen = "YES"
            vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(vc, animated: true, completion: nil)
            
        }
        else{
            UserDefaults.standard.removeObject(forKey: "user_name")
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.removeObject(forKey: "isRemembered")
            UserDefaults.standard.removeObject(forKey: "userName")
        }

    }
    
    
    
    
    @IBAction func btn_LoginAction(_ sender: Any) {
        
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController

   
        if (TextF_UserName.text != "" && TextF_Password.text != "") {
            spinner.startAnimating()
            Alamofire.request("\(baseURL)account?username=\(user)&password=\(pass)").responseJSON {[weak self] response in
               // guard let strongSelf = self else {return}
                if let json = response.result.value as? [String: Any]{
                    if (json["response"] as! NSString).intValue == 1{
                        let aux = json["data"] as! [String: Any]
                        let user_id = aux["user_id"] as! String
                        let client_id = aux["client_id"] as! String
                        let user_name = aux["username"] as! String
                        vc.userId = user_id
                        vc.clientId = client_id
                        vc.user_name = user_name
                        vc.isLoginScreen = "YES"
                        UserDefaults.standard.set(user_id, forKey: "user_id")
                        UserDefaults.standard.set(client_id, forKey: "client_id")
                        UserDefaults.standard.set(user_name, forKey: "userName")
                        
                        if self!.switchValue {
                            UserDefaults.standard.set(true, forKey: "isRemembered")
                        }
                        else{
                            UserDefaults.standard.set(false, forKey: "isRemembered")

                        }
                        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

                        self!.present(vc, animated: true, completion: nil)
                        
                    } else {
                       // strongSelf.spinner!.stopAnimating()
                        self!.spinner.stopAnimating()
                        let alert = UIAlertController(title: "Error", message: "Invalid username/password", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                          //  guard let strongSelf = self else {return}
                           // strongSelf.userTextfield.text = ""
                           // strongSelf.passTextfield.text = ""
                            self?.TextF_Password.text = ""
                            self?.TextF_Password.text = ""
                            
                        }))
                        //strongSelf.present(alert, animated: true, completion: nil)
                        self!.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
            }
        
    }
    
    
 
    @objc func userDidChange(_ textField: UITextField) {
        user = textField.text!
        user = user.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    @objc func passDidChange(_ textField: UITextField) {
        pass = textField.text!
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(ViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
}

