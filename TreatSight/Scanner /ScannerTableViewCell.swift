//
//  ScannerTableViewCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 02/05/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import iOSDropDown

class ScannerTableViewCell: UITableViewCell {

    @IBOutlet weak var dropDown: DropDown!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
