//
//  Questions_VC.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 16/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
import SVProgressHUD


class Questions_VC: UIViewController {

    @IBOutlet weak var tblView_ques_ans: UITableView!
    var arr_ViewController : NSMutableArray? = []
//    var currentIndex : Int?
//    var machineValue: String
    var client_id: String = ""
    var user_id: String = ""
    var treatpointId: String = ""
    var productId: String = ""
    var arr_questions : [[String : Any]] = [[:]]
    var arr_results : [[String : Any]] = [[:]]
    var isChild_1 : Bool = false
    var isChild_2 : Bool = false
    var isChild_3 : Bool = false
    var isNextChild : Bool = false
    var isCurrentChild : Bool = false
    var isNextChild_1 : Bool = false
    var isNextChild_2 : Bool = false
    var isNextChild_3 : Bool = false
    var isCurrentChild_1 : Bool = false
    var isCurrentChild_2 : Bool = false
    var isCurrentChild_3 : Bool = false


    var countPrevious : Int = 0
    var child_1_index : Int = 0
    var child_2_index : Int = 0
    var child_3_index : Int = 0

    var section_count : Int = 0
    var inner_section : Int = 0


    @IBOutlet weak var btn_save: UIButton!
    
//    init(_ client_id: String, _ user: String,arrViewM:NSMutableArray) {
//        self.client_id = client_id
//        self.user_id = user
//        self.arr_ViewController = arrViewM
//        super.init(nibName: nil, bundle: nil)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print("client_id",self.client_id)
//        print("user_id",self.user_id)

        self.getQuestions()
        self.registerAllCell()
    }
    
    func registerAllCell(){

        let bundle = Bundle(for: type(of: self))
        var cellNib = UINib(nibName: "GrandChild_Input_Cell", bundle: bundle)
        tblView_ques_ans.register(cellNib, forCellReuseIdentifier: "GrandChild_Input_Cell")

        cellNib = UINib(nibName: "GrandChild_Checked_Cell", bundle: bundle)
        tblView_ques_ans.register(cellNib, forCellReuseIdentifier: "GrandChild_Checked_Cell")

        cellNib = UINib(nibName: "GrandChild_Label_Cell", bundle: bundle)
        tblView_ques_ans.register(cellNib, forCellReuseIdentifier: "GrandChild_Label_Cell")

    }

    
            func getQuestions(){
        //        http://api.treatsight.com/api/ClientQuestions?user_id=8774ae79-91fb-4bcf-9db3-048f2bb0e693&client_id=ff32287f-987b-49ed-94c3-8b58297d404c
                
        //        var baseURL: String = "http://api.treatsight.com/api/"
//                print("\(baseURL)ClientQuestions?user_id=\(user_id)&client_id=\(client_id)")
                SVProgressHUD.show()
                Alamofire.request("\(baseURL)ClientQuestions?user_id=\(user_id)&client_id=\(client_id)").responseJSON {[weak self] response in
                    if let json = response.result.value as? [String: Any]{
                        if (json["response"] as! NSString).intValue == 1{
                            let aux = json["data"] as! [[String: Any]]
                            self?.arr_questions = aux
                            var loop_outer : Int = 0
                            for dic in self!.arr_questions{
                                let arr_options = dic["options"] as! [[String: Any]]
                                if arr_options.count > 0{
                                    var newArr : [Bool] = []
                                    for i in 0...arr_options.count - 1{
                                        let dic_inner = arr_options[i] as NSDictionary
                                        //                                    let dic_options = dic_inner["options"] as! NSDictionary
                                        let strQues_type : String = dic_inner.value(forKey: "cl_question_type") as! String
                                        if strQues_type == "C"{
                                            newArr.append(false)
                                        }
                                        //------------------- Level 1 ---------------------------
                                        //-------------------------------------------------------
                                        if let arr_child = dic_inner["child"] as? [[String: Any]]{
                                            var arr_child_bool_1 : [Bool] = []
                                            var loop_outer_1 : Int = 0
                                            
                                            for dictionary_inner_1 in arr_child{
                                                let strQues_type = dictionary_inner_1["cl_question_type"] as! String
                                                //                                            let strQues_type : String = dictionary_inner.value(forKey: "cl_question_type") as! String
                                                if strQues_type == "C"{
                                                    arr_child_bool_1.append(false)
                                                }
                                                
                                                //--------------------------- Level 2 ----------------------------
                                                //*****************************************************************
                                                if let arr_child = dictionary_inner_1["child"] as? [[String: Any]]{
                                                    var arr_child_bool_2 : [Bool] = []
                                                    var loop_outer_2 : Int = 0
                                                    for dictionary_inner_2 in arr_child{
                                                        //                                                let dic_child = dictionary_inner["child"] as! NSDictionary
                                                        let strQues_type = dictionary_inner_2["cl_question_type"] as! String
                                                        if strQues_type == "C"{
                                                            arr_child_bool_2.append(false)
                                                        }
                                                        //--------------------------- Level 3 ----------------------------
                                                        //*****************************************************************
                                                        if let arr_child = dictionary_inner_2["child"] as? [[String: Any]]{
                                                            var arr_child_bool_3 : [Bool] = []
                                                            var loop_outer_3 : Int = 0
                                                            for dictionary_inner_3 in arr_child{
                                                                //                                                let dic_child = dictionary_inner["child"] as! NSDictionary
                                                                let strQues_type = dictionary_inner_3["cl_question_type"] as! String
                                                                if strQues_type == "C"{
                                                                    arr_child_bool_3.append(false)
                                                                }
                                                                loop_outer_3 += 1
                                                            }
                                                            defaultss.set(arr_child_bool_3, forKey: String("\(loop_outer_2)\(loop_outer_3)"))
                                                            defaultss.synchronize()
                                                            loop_outer_2 += 1
                                                            
                                                            //------------------- Level 3 end -----------------------
                                                            
                                                        }
                                                        //------------------- Level 2 end -----------------------
                                                        
                                                    }
                                                    defaultss.set(arr_child_bool_2, forKey: String("\(loop_outer_1)\(loop_outer_2)"))
                                                    defaultss.synchronize()
                                                    loop_outer_1 += 1
                                                }
                                            }
                                            //------------------- Level 1 end -----------------------
                                            defaultss.set(arr_child_bool_1, forKey: String("\(loop_outer)\(loop_outer_1)"))
                                            defaultss.synchronize()
                                            
                                        }
                                    }
                                    //------------------- Section End -----------------------
                                    defaultss.set(newArr, forKey: String(loop_outer))
                                    defaultss.synchronize()
                                    loop_outer += 1
                                }
                            }
                    //----------------- Section Loop End ------------------
                            self!.tblView_ques_ans.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
        //MARK:- API
        //MARK:-
    
    func save_formsData_API(){

        // * Chechk Whether Network is Unreachable *
        if !(NetworkManager.sharedInstance.reachability.connection != .unavailable){
            self.view.MyToast()
            return
        }
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading..."
        let jsonString = json(from: self.arr_results)
        let parameters: [String:Any]? = ["cqr_response":jsonString as Any,
                                         "cqr_client_id":self.client_id,
                                         "cqr_user_id":self.user_id]
        let strUrl = "http://api.treatsight.com/api/ClientQuestions/Save" as String
        Alamofire.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            
            progressHUD.hide(animated: true)
            switch response.result {
            case .success:
                if let value = response.result.value {
//                    print(value)
                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        if self!.btn_save.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                        }
                        else{
                            
                        }//else
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            case .failure(let error):
                DispatchQueue.main.async() {
                    progressHUD.hide(animated: true)
//                    print(error.localizedDescription)
                }
//                print(error)
            }
        }
    }
        // -------- End Of API ---------

    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }


    @IBAction func save_Action(_ sender: Any) {
        
        self.arr_results = self.arr_questions

//        for i in 0 ... self.arr_questions.count{
//            defaultss.removeObject(forKey: String(i))
//        }
        
        var loop : Int = 0
        for dic in self.arr_questions{
            var dic_outer : [String:Any] = [:]
            var arrOptions : [[String:Any]] = [[:]]
            let dic_ques = dic["question"] as! NSDictionary
            dic_outer["question"] = dic_ques
            
            let strQues_type : String = dic_ques.value(forKey: "cl_question_type") as! String
            if strQues_type == "C" || strQues_type == "R"{
                let storeArr:[Bool] = defaultss.value(forKey: String(loop)) as! [Bool]

//                print("Check and radio")

                arrOptions = dic["options"] as! [[String: Any]]
                for i in 0...arrOptions.count - 1{
                    
                    
                    let val : Bool = storeArr[i]
                    var dic_inner = arrOptions[i]
                    dic_inner["value"] = val
                    arrOptions[i] = dic_inner
                }
            }
            else{
//                print("textField")
                let storeArr:[String] = defaultss.value(forKey: String(loop)) as! [String]

                var arrOptions = dic["options"] as! [[String: Any]]
                
                for i in 0...arrOptions.count - 1{
                    let indexPath = IndexPath(row: i, section: loop)
                    let cell = self.tblView_ques_ans.dequeueReusableCell(withIdentifier: "Input_Cell") as! Input_Cell
                    
//                    let cell: Input_Cell = self.tblView_ques_ans.cellForRow(at: indexPath) as! Input_Cell

                    let val : String = cell.txtField_answer.text!
//                    print("textField",val)
                    var dic_inner = arrOptions[i]
                    dic_inner["value"] = val
                    arrOptions[i] = dic_inner
                    
                }
            }
            var dic_main :[String:Any] = [:]
            dic_main["question"] = dic_outer["question"] as! [String : Any]
            dic_main["options"] = arrOptions
            
            self.arr_results[loop] = dic_main
//            print("Main Array",self.arr_results)
            loop += 1
            //End of outer loop
        }
        
        var arr_results1 : [[String : Any]] = [[:]]
        arr_results1 = self.arr_results
        self.save_formsData_API()
//        self.dismiss(animated: true, completion: nil)
    }
    
}


extension Questions_VC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arr_questions.count
    }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            var section_val : Int = 0
            //------------------------------------------------------
//            for dic in self.arr_questions{
            let dic = self.arr_questions[section]
                if let arr_options = dic["options"] as? [[String: Any]]{
//                    var newArr : [Bool] = []
                    for i in 0...arr_options.count - 1{
                        let dic_inner = arr_options[i] as NSDictionary
                        if let arr_child_1 = dic_inner["child"] as? [[String: Any]]{
                            if arr_child_1.count > 0{
                            for loop_1 in 0...arr_child_1.count - 1{
                                let dic_inner_2 = arr_child_1[loop_1] as NSDictionary
                                if let arr_child_2 = dic_inner_2["child"] as? [[String: Any]]{
                                    if arr_child_2.count > 0{
                                        for loop_2 in 0...arr_child_2.count - 1{
                                            let dic_inner_3 = arr_child_2[loop_2] as NSDictionary
                                            if let arr_child_3 = dic_inner_3["child"] as? [[String: Any]]{
                                                section_val += arr_child_3.count
                                            }
                                            //-------------- Level 2 end -------------
                                            section_val += 1
                                        }
                                    } //chechk the array count level 2
                                }
                                //-------------- Level 1 end -------------
                                section_val += 1
                            }
                        }
                    }//chechk the array count level 1
                        section_val += 1
                    }
                }
//                section_val += 1
//            }
//            print("index section : \(section)")
//            print("array count : \(section_val)")
            return section_val
        }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var question_type = String()
        var tempInnerSection : Int = 0
        var mainDic : [String:Any] = [:]
        print("$$$$$$$$$$$$$$$$",indexPath.section)
        print("!!!!!!!!!!!!!!!!!!",indexPath.row)
        if section_count < indexPath.section{
            inner_section = 0
            child_3_index = 0
            child_1_index = 0
            child_2_index = 0
            section_count = indexPath.section
        }

        //        var isChild : Bool = false
        //It contains section index value
        guard let dic = self.arr_questions[indexPath.section] as? [String:Any]else {
        }

        //It contains options array value
        if isCurrentChild_1{
            if let arr_options = dic["options"] as? [[String:Any]]{
                let newDic = arr_options[inner_section]
                print("*********************inner_section************************",inner_section)
                // Now, you have to check child status is it null or not
                //---------------------- Level 1 --------------------------
                if let arr_child_1 = newDic["child"] as? [[String:Any]]{
                    if arr_child_1.count > 0{
                        isChild_1 = true
                        if child_1_index == arr_child_1.count - 1{
                            isCurrentChild_1 = false
                        }
                        mainDic = arr_child_1[child_1_index]
                        if isCurrentChild_1{
                            if let arr_next_child = mainDic["child"] as? [[String:Any]],arr_next_child.count > 0{
                                isCurrentChild_2 = true
                            }//
                            else{
                                isCurrentChild_2 = false
                            }
                        if let question_type1 = mainDic["cl_question_type"] as? String{
                            question_type = question_type1
                            if question_type == "C"{
                                let cell = tableView.dequeueReusableCell(withIdentifier: "ChildChecked_Cell") as! ChildChecked_Cell
                                if let option = mainDic["cq_option"] as? String{
                                    cell.delegate = self
                                    cell.lbl_value.text = option
                                    if let arr_inner = defaultss.value(forKey: String("\(indexPath.section)\(inner_section)")) as? [Bool]{
//                                        print("---------------------\(arr_inner.count)")
                                        if arr_inner[child_1_index]{
                                            cell.btn_checked.setImage(UIImage(named: "checked_blue2"), for: .normal)
                                        }
                                        else{
                                            cell.btn_checked.setImage(UIImage(named: "uncheked_blue"), for: .normal)
                                        }
                                    }
                                }
                                
                                child_1_index += 1
//                                print("child 1 index",child_1_index)
                                return cell
                            }
                            else{
                                let cell = tableView.dequeueReusableCell(withIdentifier: "ChildLabel_Cell") as! ChildLabel_Cell
                                    if let option = mainDic["cq_option"] as? String{
                                        cell.lbl_value.text = option
                                    }
                                
                                child_1_index += 1
//                                print("child 1 index",child_1_index)
                                return cell
                            }
                        }
                            
                    }//if iscurrentchild 1
                        //---------------------- End of else --------------------
                        //---------------------- Level 2 --------------------------
                        if let arr_child_2 = mainDic["child"] as? [String:Any],isNextChild_2{
                            if arr_child_2.count > 0{
                                isChild_2 = true
                                if child_2_index == arr_child_2.count - 1{
                                    isCurrentChild_2 = false
                                }
                                let mainDic_2 = arr_child_1[child_2_index]
                                if isCurrentChild_2{
                                    if let arr_next_child = mainDic_2["child"] as? [[String:Any]],arr_next_child.count > 0{
                                        isCurrentChild_3 = true
                                    }
                                    else{
                                        isCurrentChild_3 = true
                                    }
                                if let question_type1 = mainDic_2["cl_question_type"] as? String{
                                    question_type = question_type1
                                    if question_type == "C"{
                                        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildChecked_Cell") as! ChildChecked_Cell
                                        if let option = mainDic_2["cq_option"] as? String{
                                            cell.delegate = self
                                            cell.lbl_value.text = option
                                            if let arr_inner = defaultss.value(forKey: String("\(child_1_index)\(child_2_index)")) as? [Bool]{
//                                                print("---------------------\(arr_inner.count)")
                                                if arr_inner[child_2_index]{
                                                    cell.btn_checked.setImage(UIImage(named: "checked_blue2"), for: .normal)
                                                }
                                                else{
                                                    cell.btn_checked.setImage(UIImage(named: "uncheked_blue"), for: .normal)
                                                }
                                            }
                                        }
                                        child_2_index += 1
//                                        print("child 2 index",child_2_index)
                                        return cell
                                    }
                                    else{
                                        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildLabel_Cell") as! ChildLabel_Cell
                                            if let option = mainDic_2["cq_option"] as? String{
                                                cell.lbl_value.text = option
                                            }
                                        child_2_index += 1
//                                        print("child 2 index",child_2_index)
                                        return cell
                                    }
                                }
                            }
                                //---------------------- End of else --------------------
                                //---------------------- Level 3 --------------------------
                                if let arr_child_3 = mainDic["child"] as? [String:Any],isNextChild_3,arr_child_3.count > 0{
                                    isChild_3 = true
                                    if child_3_index == arr_child_3.count - 1{
                                        isCurrentChild_3 = false
                                    }
                                        if let mainDic_3 = arr_child_1[child_3_index] as? [String:Any]{
                                            if let question_type1 = mainDic_3["cl_question_type"] as? String{
                                                question_type = question_type1
                                                if question_type == "C"{
                                                    let cell = tableView.dequeueReusableCell(withIdentifier: "ChildChecked_Cell") as! ChildChecked_Cell
                                                    if let option = mainDic_3["cq_option"] as? String{
                                                        cell.delegate = self
                                                        cell.lbl_value.text = option
                                                        if let arr_inner = defaultss.value(forKey: String("\(child_2_index)\(child_3_index)")) as? [Bool]{
//                                                            print("---------------------\(arr_inner.count)")
                                                            if arr_inner[child_3_index]{
                                                                cell.btn_checked.setImage(UIImage(named: "checked_blue2"), for: .normal)
                                                            }
                                                            else{
                                                                cell.btn_checked.setImage(UIImage(named: "uncheked_blue"), for: .normal)
                                                            }
                                                        }
                                                    }
                                                    child_3_index += 1
                                                    print("child 3 index",child_3_index)

                                                    return cell
                                                }
                                                else{
                                                    let cell = tableView.dequeueReusableCell(withIdentifier: "ChildLabel_Cell") as! ChildLabel_Cell
                                                        if let option = mainDic_3["cq_option"] as? String{
                                                            cell.lbl_value.text = option
                                                        }
                                                    
                                                    child_3_index += 1
//                                                    print("child 3 index",child_3_index)
                                                    return cell
                                                }
                                                //--------------- End of else ----------------
                                            }
                                        }
                                    
                                    child_3_index += 1
//                                    print("child_3_indexindex",child_3_index)

                                }
                                child_2_index += 1
//                                print("child 2 index",child_2_index)

                            }
                            if let question_type1 = mainDic["cl_question_type"] as? String{
                                question_type = question_type1
                            }
                        }
                    }
                }
            }
        }
        else{
            if let arr_options = dic["options"] as? [[String:Any]]{
                print("##### inner_section ######",inner_section)

                let newDic = arr_options[inner_section]
//                print("inner Section :",inner_section)
                // Now, you have to check child status is it null or not
                if let question_type1 = newDic["cl_question_type"] as? String{
                    question_type = question_type1
                    mainDic = newDic
                }
                if let arr_child = newDic["child"] as? [[String:Any]],arr_child.count > 0{
                    isCurrentChild_1 = true
                }
                else{
                    isCurrentChild_1 = false
                    tempInnerSection = inner_section
                    inner_section += 1
                    child_1_index = 0
                }
            }
        }
        
        if !isCurrentChild_1 {
            child_1_index = 0
        }
        if !isCurrentChild_2 {
            child_2_index = 0
        }
        if !isCurrentChild_3 {
            child_3_index = 0
        }

        if question_type == "N"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Input_Cell") as! Input_Cell
            if let dic = mainDic as? [String:Any]{
                //                if let arr_option = dic["options"] as? NSArray{
                //                    let dic_option = arr_option[indexPath.row] as? NSDictionary
                if let option = dic["cq_option"] as? String{
                    cell.lbl_option.text = option
                }
            }
            return cell
            
        }
        else
            if question_type == "C"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Checkbox_Cell") as! Checkbox_Cell
                if let option = mainDic["cq_option"] as? String{
                    cell.delegate = self
                    cell.lbl_value.text = option
                    if let arr_inner = defaultss.value(forKey: String(indexPath.section)) as? [Bool]{
//                        print("---------------------\(arr_inner.count)")
                        if arr_inner[tempInnerSection]{
                            cell.btn_checkBox.setImage(UIImage(named: "checked_blue2"), for: .normal)
                        }
                        else{
                            cell.btn_checkBox.setImage(UIImage(named: "uncheked_blue"), for: .normal)
                        }
                    }
                }
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Questions_Cell") as! Questions_Cell
                if let option = mainDic["cq_option"] as? String{
                    cell.lbl_question.text = option
                }
                return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var question_type = String()
        var mainDic : [String:Any] = [:]

        guard let dic = self.arr_questions[indexPath.section] as? [String:Any]else {
        }
//        if let arr_options = dic["options"] as? [[String:Any]]{
//            let newDic = arr_options[indexPath.row]
////            mainDic = newDic["options"] as! [String:Any]
//
//            //            let ques : String = mainDic["cl_questions"] as! String
//            question_type = newDic["cl_question_type"] as! String
//            if let arr_child = newDic["child"] as? [[String:Any]]{
////                isChild = true
//                let innerDic = arr_child[indexPath.row]
//                mainDic = innerDic["child"] as! [String:Any]
//
//
//            }
//
//        }
//
//        if let question_type1 = newDic["cl_question_type"] as? String{
//            question_type = question_type1
//        }
//
//        if question_type == "C" || question_type == "L"{
//            return 50
//        }
//        else if question_type == "N"{
//            return 94
//        }
//        else{
            return 50
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Section_Ques_Cell") as! Section_Ques_Cell
        guard let dic = self.arr_questions[section] as? [String:Any]else {
       }
        if let dic_question = dic["question"] as? [String:Any]{
            let ques : String = dic_question["cl_questions"] as! String
            let strValue : String = "* " + ques
             cell.lbl_title.text = strValue
        }
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 54
    }
    
}

extension Questions_VC : CheckBox_Cell_Delegate{
    
    func checkedTapped(cell: Checkbox_Cell) {
        let indexPath = self.tblView_ques_ans.indexPath(for: cell)
        let section : Int = indexPath!.section
        if let arr_inner = defaultss.value(forKey: String(indexPath!.section)) as? [Bool]{
            var newArr = arr_inner
//            print("---------------------\(arr_inner.count)")
            var val = arr_inner[indexPath!.row]
            val = !val
            if val{
                cell.btn_checkBox.setImage(UIImage(named: "checked_blue2"), for: .normal)
            }
            else{
                cell.btn_checkBox.setImage(UIImage(named: "uncheked_blue"), for: .normal)//uncheked_blue
            }
            newArr[indexPath!.row] = val
//            defaultss.set(newArr, forKey: String(loop))

            defaultss.set(newArr, forKey: String(section))
            defaultss.synchronize()
        }
        
    }
}


extension Questions_VC : Radio_Cell_Delegate{

    
    func radioTapped(cell: Radio_Cell) {
        let indexPath = self.tblView_ques_ans.indexPath(for: cell)
        let section : Int = indexPath!.section
        if let arr_inner = defaultss.value(forKey: String(indexPath!.section)) as? [Bool]{
            var newArr = arr_inner
//            print("---------------------\(arr_inner.count)")
//            var val = arr_inner[indexPath!.row]
//            val = !val
            for i in 0...newArr.count - 1{
                if indexPath!.row == i{
                    cell.btn_radio.setImage(UIImage(named: "radio_checked"), for: .normal)//radio_checked
                    newArr[i] = true
                }
                else{
                    cell.btn_radio.setImage(UIImage(named: "radio_unchecked"), for: .normal)
                    newArr[i] = false
                }
            }
            
            defaultss.set(newArr, forKey: String(section))
            defaultss.synchronize()
        }
        
        self.tblView_ques_ans.reloadData()
    }
}




    
    extension Questions_VC : Child_Checked_Cell_Delegate{
        
        func childCheckedTapped(cell: ChildChecked_Cell) {
            let indexPath = self.tblView_ques_ans.indexPath(for: cell)
            let section : Int = indexPath!.section
            if let arr_inner = defaultss.value(forKey: String(indexPath!.section)) as? [Bool]{
                var newArr = arr_inner
//                print("---------------------\(arr_inner.count)")
                var val = arr_inner[indexPath!.row]
                val = !val
                if val{
                    cell.btn_checked.setImage(UIImage(named: "checked_blue2"), for: .normal)
                }
                else{
                    cell.btn_checked.setImage(UIImage(named: "uncheked_blue"), for: .normal)//uncheked_blue
                }
                newArr[indexPath!.row] = val
    //            defaultss.set(newArr, forKey: String(loop))

                defaultss.set(newArr, forKey: String(section))
                defaultss.synchronize()
            }
            
        }
    }
