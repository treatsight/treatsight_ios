//
//  CustomCheckBoxCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 01/05/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit
import SimpleCheckbox
class CustomCheckBoxCell: UITableViewCell {

    
    @IBOutlet weak var checked_1: Checkbox!
    @IBOutlet weak var checked_2: Checkbox!
    @IBOutlet weak var checked_3: Checkbox!
    @IBOutlet weak var checked_4: Checkbox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
