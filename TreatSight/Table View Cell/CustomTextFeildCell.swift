//
//  CustomTextFeildCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 01/05/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit

class CustomTextFeildCell: UITableViewCell {
    
    @IBOutlet weak var txtFeild_sectionValue: UITextField!
    
    @IBOutlet weak var lbl_sectionName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
