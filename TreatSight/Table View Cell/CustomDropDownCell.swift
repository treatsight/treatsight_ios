//
//  CustomDropDownCell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 01/05/19.
//  Copyright © 2019 Coding Brains. All rights reserved.
//

import UIKit
import iOSDropDown
class CustomDropDownCell: UITableViewCell {

    @IBOutlet weak var lbl_section_name: UILabel!
    
    @IBOutlet weak var drop_down: DropDown!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
