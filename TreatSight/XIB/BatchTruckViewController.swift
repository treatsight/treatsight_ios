//
//  BatchTruckViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 16/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown
import Alamofire
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD

class BatchTruckViewController: UIViewController {
    
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    
    var machineId: String
    var clientId: String
    var userId: String
    var treatpointId: String
    var treatpointValue: String = ""
    var productValue: String = ""
    var dateValue: String = ""
    var commentsValue: String = ""
    
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""
    
    var preValue: Float = 0.0
    var volumeValue: Float = 0.0
    var postValue: Float = 0.0
    
    
    var preValue_inch: Float = 0
    var volumeValue_inch: Float = 0
    var postValue_inch: Float = 0
    var gals: Float64 = 0.00
    var inches: Float64 = 0.00

    
    var products =  Dictionary<String, String>()
    var treatpoints =  Dictionary<String, String>()
    
    
    @IBOutlet weak var postVolume_inch: UITextField!
    @IBOutlet weak var preflush_inch: UITextField!
    
    @IBOutlet weak var chemicalVolume_inch: UITextField!
    @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var treatpointList: DropDown!
    @IBOutlet weak var productList: DropDown!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var commentsTextfield: UITextView!
    @IBOutlet weak var preVolumeTextfield: UITextField!
    @IBOutlet weak var volumeTextfield: UITextField!
    @IBOutlet weak var postVolumeTextfield: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var dropdown_comment: DropDown!
    var final_comment_value: String = ""

    @IBOutlet weak var treatpointLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    
    
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
//        arrayCount = arrayCount
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        
        dropdown_comment.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.comment_value = strongSelf.comments_array[string]!
            
        }
        dropdown_comment.delegate = self
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
    
        
        
        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment

        strComment = ""
        commentsTextfield.layer.borderColor = UIColor.darkGray.cgColor
        commentsTextfield.layer.borderWidth = 1.0
        commentsTextfield.layer.cornerRadius = 4

        
    }
  
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        commentsTextfield.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        strComment = ""
        strComment = commentsTextfield.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        commentsTextfield.text  = strComment
        final_comment_value = commentsTextfield.text!
    }
    
    
    init(_ machineId: String,_ tp_Id: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.treatpointId = tp_Id
        self.machineId = machineId
        self.arr_ViewController = arrViewM
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.treatpointLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.treatpointLabel.layer.borderWidth = 1.0
        self.treatpointLabel.layer.cornerRadius = 5.0
        
        self.productLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.productLabel.layer.borderWidth = 1.0
        self.productLabel.layer.cornerRadius = 5.0
        
        
        
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        
//        commentsTextfield.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        commentsTextfield.inputAccessoryView = getToolbar()
        preVolumeTextfield.addTarget(self, action: Selector(("preDidChange:")), for: UIControl.Event.editingChanged)
        preVolumeTextfield.inputAccessoryView = getToolbar()
        
        preflush_inch.addTarget(self, action: Selector(("preDidChange_inch:")), for: UIControl.Event.editingChanged)
        preflush_inch.inputAccessoryView = getToolbar()

        
        volumeTextfield.addTarget(self, action: Selector(("volumeDidChange:")), for: UIControl.Event.editingChanged)
        volumeTextfield.inputAccessoryView = getToolbar()
        
        chemicalVolume_inch.addTarget(self, action: Selector(("volumeDidChange_inch:")), for: UIControl.Event.editingChanged)
        chemicalVolume_inch.inputAccessoryView = getToolbar()
        
        postVolumeTextfield.addTarget(self, action: Selector(("postDidChange:")), for: UIControl.Event.editingChanged)
        postVolumeTextfield.inputAccessoryView = getToolbar()
        
        postVolume_inch.addTarget(self, action: Selector(("postDidChange_inch:")), for: UIControl.Event.editingChanged)
        postVolume_inch.inputAccessoryView = getToolbar()
        
        /*
        treatpointList.delegate = self
        treatpointList.selectedRowColor = themeColor()
        dropdown_comment.selectedRowColor = themeColor()

        productList.delegate = self
        productList.selectedRowColor = themeColor()

        
        treatpointList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.treatpointValue = strongSelf.treatpoints[string]!
            
        }
        
        productList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.productValue = strongSelf.products[string]!
            
        }
        */
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
         self.dateValue = myString
        
        NotificationCenter.default.addObserver(self, selector: #selector(BatchTruckViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BatchTruckViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        SVProgressHUD.show()
        Alamofire.request("\(baseURL)ctm?user_id=\(userId)&client_id=\(clientId)&machineid=\(treatpointId)&get_treatpoints").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                    /*
                    var treatpointsResults: [String] = []
                    for treatpoint in treatpoints {
                        let value:String = treatpoint["treatpoint_id"] as! String
                        let name: String = treatpoint["treatpoint_name"] as! String
                        
                        self!.gals = treatpoint["strappings_gals"] as! Float64
                        self!.inches = treatpoint["strappings_inches"] as! Float64

                        
                        treatpointsResults.append(name)
                        strongSelf.treatpoints[treatpoint["treatpoint_name"] as! String] = value
                    }*/
                    SVProgressHUD.dismiss()
                    //strongSelf.treatpointList.optionArray = treatpointsResults
                    //                    strongSelf.treatPointList.optionIds = treatpointsIds
                    //strongSelf.treatpointList.selectedIndex = 0
                    
                    self!.gals = treatpoints[0]["strappings_gals"] as! Float64
                    self!.inches = treatpoints[0]["strappings_inches"] as! Float64
                    
                    strongSelf.treatpointValue = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.productValue = treatpoints[0]["product_id"] as! String
                    
                    strongSelf.treatpointLabel.text = " "+String(treatpoints[0]["treatpoint_name"] as! String) + " "
                    strongSelf.productLabel.text = " "+String(treatpoints[0]["product_name"] as! String)+" "
                    
                    //strongSelf.treatpointList.text = treatpointsResults[0]
                    
                }
            }
        }
        
       /* SVProgressHUD.show()
        Alamofire.request("\(baseURL)product").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    
                    var productsResults: [String] = []
                    for product in products {
                        productsResults.append(product["product_name"]!)
                        strongSelf.products[product["product_name"]!] = product["product_id"]
                    }
                    SVProgressHUD.dismiss()
                    strongSelf.productList.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.productList.selectedIndex = 0
                    strongSelf.productValue = products[0]["product_id"]!
                    strongSelf.productList.text = productsResults[0]

                }
            }
        }//Product Dropdown End
        */
    }

    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
        
        if (self.productValue != "" && self.dateValue != "" && self.treatpointValue != "" ) {
            self.saveButton.isEnabled = true
            self.saveButton.alpha = 1
        } else {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.5
        }
    }
    
    @objc func commentsDidChange(_ textField: UITextField) {
        self.commentsValue = textField.text!
    }
    
    @objc func preDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.preValue = 0; return}
        self.preValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.preValue_inch = self.preValue*Float((inches/gals))
        self.preflush_inch.text = String(format: "%.2f", self.preValue_inch)
        print(self.preValue_inch)
        print(self.preflush_inch.text as Any)
        }
    }
    
    @objc func volumeDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.volumeValue = 0; return}
        self.volumeValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.volumeValue_inch = self.volumeValue*Float((inches/gals))
        self.chemicalVolume_inch.text = String(format: "%.2f", self.volumeValue_inch)
        print(self.volumeValue_inch)
        print(self.chemicalVolume_inch.text as Any)
        }

    }
    
    @objc func postDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.postValue = 0; return}
        self.postValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.postValue_inch = self.postValue*Float((inches/gals))
        self.postVolume_inch.text = String(format: "%.2f", self.postValue_inch)
        print(self.postValue_inch)
        print(self.postVolume_inch.text as Any)
        }
    }
    
    @objc func preDidChange_inch(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.preValue_inch = 0; return}
        self.preValue_inch = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.preValue = self.preValue_inch*Float((gals/inches))
        self.preVolumeTextfield.text = String(format: "%.2f", self.preValue)
        print(self.preValue)
        print(self.preVolumeTextfield.text as Any)
        }
    }
    
    @objc func volumeDidChange_inch(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.volumeValue_inch = 0; return}
        self.volumeValue_inch = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.volumeValue = self.volumeValue_inch*Float((gals/inches))
        self.volumeTextfield.text = String(format: "%.2f", self.volumeValue)
        print(self.volumeValue)
        print(self.volumeTextfield.text as Any)
        }
    }
    
    @objc func postDidChange_inch(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.postValue_inch = 0; return}
        self.postValue_inch = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
            self.postValue = self.postValue_inch*Float((gals/inches))
        self.postVolumeTextfield.text = String(format: "%.2f", self.postValue)

        print(self.postValue)
        print(self.postVolumeTextfield.text as Any)
        }

    }

    
    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(BatchTruckViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func save(_ sender: Any) {
        SVProgressHUD.show()
        final_comment_value = commentsTextfield.text!
        let EntryUrl = "\(baseURL)Treatment?&treatpoint_id=\(treatpointValue)&product_id=\(productValue)&treatment_date=\(dateValue)&last_updated_by=\(clientId)&comments=\(final_comment_value)&pre_flush_volume=\(preValue)&volume=\(volumeValue)&post_flush_volume=\(postValue)&pre_flush_volume_level=\(preValue_inch)&post_flush_volume_level=\(postValue_inch)&volume_level=\(volumeValue_inch)&invoice_total==\(0)&invoice_num=\(0)&line_num==\(0)save_bandt_form_value=1&machine_id=\(machineId)&user_id=\(userId)"
        
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        
        Alamofire.request(encodedUrl!).responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}
                        
                        //------------------ Gaurav Code --------------------

                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                        }
                        else{
                            if let val :String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1
                        switch val {
                        case "form_3":
                            let vc = MonitoringPlanViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController!)
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_4":
                            let vc = AnalysisViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            break
                        case "form_5":
                            let vc = WellFailureViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                            
                        case "form_6":
                            let vc = SamplingViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_7":
                            let vc = WellTestViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)

                            break
                        case "form_8":
                            let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_9":
                            let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_10":
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                            vc.client_id = strongSelf.clientId
                            vc.user_id = strongSelf.userId
                            vc.arr_ViewController = strongSelf.arr_ViewController!
                            strongSelf.present(vc, animated: true, completion: nil)
                        default:
                            break
                        }
                            }//If Let
                        }//else
                        //------------------ End --------------------
                        
                        
                        
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
        
    }
    @IBAction func goBack(_ sender: Any) {
//        self.present(ScannerViewController(clientId,userId,"batch-truck"), animated: true, completion: nil)
    }
}


extension BatchTruckViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
