//
//  KnowledgeViewController.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 04/05/19.
//  Copyright © 2019. All rights reserved.
//


import Foundation
import UIKit
import Alamofire
import iOSDropDown
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD
//import AFNetworking
import SVProgressHUD

class KnowledgeViewController: UIViewController {
    
    
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineValue: String
    var client_id: String = ""
    var user_id: String = ""
    var treatpointId: String = ""
    var productId: String = ""
    var dateValue: String!
    var imgData  = Data()
    var picker = UIImagePickerController()
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""
    var treatpoints =  Dictionary<String, String>()
    var actualHeight : CGFloat = 0.0
    var treatpointvalue: String = ""
    var category: String = ""
    var imageView:UIImageView? = nil
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var cons_imgHeight: NSLayoutConstraint!
    @IBOutlet weak var img_knowledge: UIImageView!
    @IBOutlet weak var btn_save: UIButton!
    @IBOutlet weak var lbl_formCount: UILabel!
    
    @IBOutlet weak var treatpointLabel: UILabel!
    
    @IBAction func save(_ sender: Any) {
        self.updateProfileAPI()
    }
    
    /*
    func updateProfileAPI()
    {
        
        SVProgressHUD.show()
        category = txtFeild_category.text!
        let url = "\(baseURL)knowledgepoint/save"
        let parameters = ["category": category,"associateid": treatpointId,"associatetype": treatpointvalue]
        let imageData: Data = try! img_knowledge.image!.jpegData(compressionQuality:0.5)!
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        
        manager.post(url, parameters: parameters, constructingBodyWith: { (data: AFMultipartFormData!) -> Void in
            data.appendPart(withFileData: imageData, name: "file", fileName: "photo.jpg", mimeType: "image/jpeg")
            
        }, progress: nil, success: {(operation, responseObject) in
            if let dict = responseObject as? NSDictionary {
                print(dict)
                print(dict["flag"] ?? "")
                SVProgressHUD.dismiss()
                let strMsg = dict["msg"] as? String
                //----------------------------------------------
                if (dict["response"] as! NSString).intValue == 1{
                    let alert = UIAlertController(title: "Success", message: strMsg, preferredStyle: UIAlertController.Style.alert)
     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
     self?.goToHome()
     }))
     //
     self.present(alert, animated: true, completion: nil)
     //------------------------------------------------
     }//end Of If Condition
     }
     }, failure: {
     (operation, error) in
     print("Error: " + error.localizedDescription)
     SVProgressHUD.dismiss()
     
     })
     }
     
     */
    
    
    func updateProfileAPI()
    {
        SVProgressHUD.show()
        //        category = txtFeild_category.text!
        let url = "\(baseURL)knowledgepoint/save"
        let parameters = ["category": category,"associateid": treatpointId,"associatetype": treatpointvalue]
        let imageData: Data = try! img_knowledge.image!.jpegData(compressionQuality:0.5)!
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                MultipartFormData.append(self.img_knowledge.image!.jpegData(compressionQuality: 1)!, withName: "file", fileName: "photo.jpg", mimeType: "image/jpeg")
        }, to: url) { (result) in
            
            SVProgressHUD.dismiss()
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    if self.btn_save.titleLabel?.text == saveBtnTitle_atEnd{

                        let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                                                        
                            if self!.btn_save.titleLabel?.text == saveBtnTitle_atEnd{
                                self?.goToHome()
                                
                            }
                            else{
                                if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                    counterOfViewController = counterOfViewController + 1
                                    switch val {
                                        
                                    case "form_10":
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                                        vc.client_id = self!.client_id
                                        vc.user_id = self?.user_id as! String
                                        vc.arr_ViewController = self?.arr_ViewController!
                                        self!.present(vc, animated: true, completion: nil)
                                        
                                        break
                                        
                                    default:
                                        break
                                    }//Switch
                                }// If Let
                            }//else
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else{
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                        vc.client_id = self.client_id
                        vc.user_id = self.user_id
                        vc.arr_ViewController = self.arr_ViewController!
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            case .failure(let encodingError): break
            SVProgressHUD.dismiss()
            
            print(encodingError)
            }
        }
    }
    
    
    @IBOutlet weak var dropDown_treatPoint: DropDown!
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.treatpointLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.treatpointLabel.layer.borderWidth = 1.0
        self.treatpointLabel.layer.cornerRadius = 5.0
        
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        //        arrayCount = arrayCount
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            btn_save.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }

        
        /*dropDown_treatPoint.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.treatpointId = strongSelf.treatpoints[string]!
            strongSelf.treatpointvalue = self!.dropDown_treatPoint.text!
            
        }
        dropDown_treatPoint.selectedRowColor = themeColor()
       */ Alamofire.request("\(baseURL)ctm?user_id=\(user_id)&client_id=\(client_id)&machineid=\(treatpointId)&get_treatpoints").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                    /*var treatpointsResults: [String] = []
                    for treatpoint in treatpoints {
                        let value:String = treatpoint["treatpoint_id"] as! String
                        let name: String = treatpoint["treatpoint_name"] as! String
                        treatpointsResults.append(name)
                        strongSelf.treatpoints[treatpoint["treatpoint_name"] as! String] = value
                    }*/
                    SVProgressHUD.dismiss()
                    //strongSelf.dropDown_treatPoint.optionArray = treatpointsResults
                    //                    strongSelf.treatPointList.optionIds = treatpointsIds
                    //strongSelf.dropDown_treatPoint.selectedIndex = 0
                    strongSelf.treatpointId = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.treatpointvalue = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.treatpointLabel.text = " "+" "+String(treatpoints[0]["treatpoint_name"] as! String)+" "
                    //strongSelf.dropDown_treatPoint.text = treatpointsResults[0]
                    
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount = String(counterOfViewController)
        let arrayCount:Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        actualHeight = img_knowledge.frame.height
        //        cons_imgHeight.constant = 0
        txtFeild_category.addTarget(self, action: #selector(KnowledgeViewController.categoryDidChange(_:)), for: UIControl.Event.editingChanged)
        txtFeild_category.inputAccessoryView = getToolbar()
        NotificationCenter.default.addObserver(self, selector: #selector(KnowledgeViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KnowledgeViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func categoryDidChange(_ textField: UITextField) {
        self.txtFeild_category.text = textField.text!
    }
    
    
    
    @IBAction func upload_image(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = (sender as AnyObject).bounds
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var txtFeild_category: SkyFloatingLabelTextField!
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    
    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.client_id = client_id
        self.user_id = user
        self.treatpointId = tpId
        self.machineValue = machineId
        self.arr_ViewController = arrViewM
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(KnowledgeViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    
}



//MARK: - UIImagePickerControllerDelegate
//MARK:-- ImagePicker delegate
extension KnowledgeViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            img_knowledge.image = editedImage
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
}


extension KnowledgeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        /*if ( textField == dropDown_treatPoint){
            return false
        } else {*/
            return true
       // }
    }
}
extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
