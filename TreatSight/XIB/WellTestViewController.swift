//
//  WellTestViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 17/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SkyFloatingLabelTextField
import iOSDropDown
import MBProgressHUD
import SVProgressHUD


class WellTestViewController: UIViewController {
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    var treatpointId: String
    var userId: String
    var clientId: String
    var dateValue: String = ""
    var oilRate: Float = 0
    var gasRate: Float = 0
    var waterRate: Float = 0
    var fluidLevel: Float = 0
    var surfaceTemperature: Float = 0
    var apiGravity: Float = 0
    var cassing: Float = 0
    var tubbing: Float = 0
    var comments: String = ""
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""

    var final_comment_value: String = ""

     @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var oilRateTextfield: UITextField!
    @IBOutlet weak var gasRateTextfield: UITextField!
    @IBOutlet weak var waterRateTextfield: UITextField!
    @IBOutlet weak var fluidLevelTextfield: UITextField!
    @IBOutlet weak var surfaceTemperatureTextfield: UITextField!
    @IBOutlet weak var apiGravityTextfield: UITextField!
    @IBOutlet weak var cassingTextfield: UITextField!
    @IBOutlet weak var tubbingTextfield: UITextField!
    @IBOutlet weak var commentsTextfield: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dropdown_comment: DropDown!

    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        dropdown_comment.selectedRowColor = themeColor()

        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment
        strComment = ""
        commentsTextfield.layer.borderColor = UIColor.darkGray.cgColor
        commentsTextfield.layer.borderWidth = 1.0

    }
    
    
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        commentsTextfield.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        strComment = ""
        strComment = commentsTextfield.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        commentsTextfield.text  = strComment
        final_comment_value = commentsTextfield.text!
    }

    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.treatpointId = tpId
        self.machineId = machineId
        super.init(nibName: nil, bundle: nil)
        self.arr_ViewController = arrViewM

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        
        oilRateTextfield.addTarget(self, action: #selector(AnalysisViewController.oilDidChange(_:)), for: UIControl.Event.editingChanged)
        oilRateTextfield.inputAccessoryView = getToolbar()
        gasRateTextfield.addTarget(self, action: #selector(WellTestViewController.gasDidChange(_:)), for: UIControl.Event.editingChanged)
        gasRateTextfield.inputAccessoryView = getToolbar()
        waterRateTextfield.addTarget(self, action:#selector(WellTestViewController.waterDidChange(_:)), for: UIControl.Event.editingChanged)
        waterRateTextfield.inputAccessoryView = getToolbar()
        fluidLevelTextfield.addTarget(self, action:#selector(WellTestViewController.fluidDidChange(_:)), for: UIControl.Event.editingChanged)
        fluidLevelTextfield.inputAccessoryView = getToolbar()
        surfaceTemperatureTextfield.addTarget(self, action:#selector(WellTestViewController.temperatureDidChange(_:)), for: UIControl.Event.editingChanged)
        surfaceTemperatureTextfield.inputAccessoryView = getToolbar()
        apiGravityTextfield.addTarget(self, action: #selector(WellTestViewController.apiGravityDidChange(_:)), for: UIControl.Event.editingChanged)
        apiGravityTextfield.inputAccessoryView = getToolbar()
        cassingTextfield.addTarget(self, action: #selector(WellTestViewController.cassingDidChange(_:)), for: UIControl.Event.editingChanged)
        cassingTextfield.inputAccessoryView = getToolbar()
        tubbingTextfield.addTarget(self, action: #selector(WellTestViewController.tubbingDidChange(_:)), for: UIControl.Event.editingChanged)
        tubbingTextfield.inputAccessoryView = getToolbar()
//        commentsTextfield.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        commentsTextfield.inputAccessoryView = getToolbar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(WellTestViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(WellTestViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        self.dateValue = myString

//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
    @objc func oilDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.oilRate = 0;return}
        self.oilRate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func gasDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.gasRate = 0;return}
        self.gasRate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func waterDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.waterRate = 0;return}
        self.waterRate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func fluidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.fluidLevel = 0;return}
        self.fluidLevel = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func temperatureDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.surfaceTemperature = 0;return}
        self.surfaceTemperature = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func apiGravityDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.apiGravity = 0 ;return}
        self.apiGravity = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func cassingDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.cassing = 0;return}
        self.cassing = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func tubbingDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.tubbing = 0; return}
        self.tubbing = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    @objc func commentsDidChange(_ textField: UITextField) {
        self.comments = textField.text!
    }
    
    private func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: a.date)
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.dateValue = myString

    }
    
    @IBAction func goBack(_ sender: Any) {
//        self.present(ScannerViewController(clientId,userId,"well-test"), animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        
//        Alamofire.request(encodedUrl!).responseJSON { response in
//            print("Request: \(String(describing: response.request))")   // original url request
//            print("Response: \(String(describing: response.response))") // http url response
//            print("Result: \(response.result)")                         // response serialization result
//
//            if let json = response.result.value {
//                print("JSON: \(json)") // serialized json response
//            }
//
//            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
//                print("Data: \(utf8Text)") // original server data as UTF8 string
//            }
        
        SVProgressHUD.show()
        final_comment_value = String(commentsTextfield.text)
        
        let EntryUrl = "\(baseURL)WellTests?save_welltest_form_value=1&machine_id=\(machineId)&user_id=\(userId)&test_date=\(dateValue)&oil_rate=\(oilRate)&gas_rate=\(gasRate)&water_rate=\(waterRate)&fluid_level=\(fluidLevel)&surf_temp=\(surfaceTemperature)&api_grav=\(apiGravity)&csg_press=\(cassing)&tbg_press=\(tubbing)&comments=\(final_comment_value)&last_updated_by=\(clientId)"
        
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
  
        
        
        Alamofire.request(encodedUrl!).responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{

                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}

                        //------------------ Gaurav Code --------------------


                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()

                        }
                        else{
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1

                                switch val {
                                case "form_8":
                                    let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                case "form_9":
                                    let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                case "form_10":
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                                    vc.client_id = strongSelf.clientId
                                    vc.user_id = strongSelf.userId
                                    vc.arr_ViewController = strongSelf.arr_ViewController!
                                    strongSelf.present(vc, animated: true, completion: nil)

                                    break

                                default:
                                    break
                                }//Switch
                            }// If Let
                        }//else

                        //------------------ End --------------------

                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()

                print("Error \(String(describing: response.result.error))")

            }
        }
    }
}

extension WellTestViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ( textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
