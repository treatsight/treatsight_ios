//
//  MonitoringViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 16/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import SimpleCheckbox
import iOSDropDown
import Alamofire
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD

class MonitoringPlanViewController: UIViewController {
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    var clientId: String
    var userId: String
    var treatpointId: String = ""
    var commentsValue: String = ""
    var dateValue: String = ""
    
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""

    
    var treatpoints =  Dictionary<String, String>()
    
     @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var treatpointList: DropDown!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var commentsTextfield: UITextView!
    @IBOutlet weak var phFactorCheck: Checkbox!
    @IBOutlet weak var ironTotalCheck: Checkbox!
    @IBOutlet weak var totalHardnessCheck: Checkbox!
    @IBOutlet weak var oxygenCheck: Checkbox!
    @IBOutlet weak var hydrogenCheck: Checkbox!
    @IBOutlet weak var phosophonateCheck: Checkbox!
    @IBOutlet weak var totalSolidsCheck: Checkbox!
    @IBOutlet weak var formicAcidCheck: Checkbox!
    @IBOutlet weak var aceticAcidCheck: Checkbox!
    @IBOutlet weak var propionicAcidCheck: Checkbox!
    @IBOutlet weak var buryticAcidCheck: Checkbox!
    @IBOutlet weak var valericAcidCheck: Checkbox!
    @IBOutlet weak var manganeseCheck: Checkbox!
    @IBOutlet weak var silicaCheck: Checkbox!
    @IBOutlet weak var cidCheck: Checkbox!
    @IBOutlet weak var couponCheck: Checkbox!
    @IBOutlet weak var lprCheck: Checkbox!
    @IBOutlet weak var erCheck: Checkbox!
    @IBOutlet weak var microcorrCheck: Checkbox!
    @IBOutlet weak var potentiadymeCheck: Checkbox!
    @IBOutlet weak var amineResidualCheck: Checkbox!
    @IBOutlet weak var srbCheck: Checkbox!
    @IBOutlet weak var apbCheck: Checkbox!
    @IBOutlet weak var atpCheck: Checkbox!
    @IBOutlet weak var oilCheck: Checkbox!
    @IBOutlet weak var turbidityCheck: Checkbox!
    @IBOutlet weak var carbonDioxideCheck: Checkbox!
    @IBOutlet weak var sulfateCheck: Checkbox!
    @IBOutlet weak var bicarbonateCheck: Checkbox!
    @IBOutlet weak var gabCheck: Checkbox!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dropdown_comment: DropDown!
    var final_comment_value: String = ""

    @IBOutlet weak var treatpointLabel: UILabel!
    
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
//        arrayCount = arrayCount + 1
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        dropdown_comment.selectedRowColor = themeColor()
        //treatpointList.selectedRowColor = themeColor()

        
        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment
        
        
        strComment = ""
        commentsTextfield.layer.borderColor = UIColor.darkGray.cgColor
        commentsTextfield.layer.borderWidth = 1.0

    }
    
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        commentsTextfield.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        strComment = ""
        strComment = commentsTextfield.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        commentsTextfield.text  = strComment
        final_comment_value = commentsTextfield.text!
        
    }
    
    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.treatpointId = tpId
        self.machineId = machineId
        super.init(nibName: nil, bundle: nil)
        self.arr_ViewController = arrViewM

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.treatpointLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.treatpointLabel.layer.borderWidth = 1.0
        self.treatpointLabel.layer.cornerRadius = 5.0
        
        NotificationCenter.default.addObserver(self, selector: #selector(MonitoringPlanViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MonitoringPlanViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        commentsTextfield.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        commentsTextfield.inputAccessoryView = getToolbar()
        
      /*  treatpointList.delegate = self
        treatpointList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.treatpointId = strongSelf.treatpoints[string]!
            
        }
        */
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
          self.dateValue = myString        
        SVProgressHUD.show()
        Alamofire.request("\(baseURL)ctm?user_id=\(userId)&client_id=\(clientId)&machineid=\(treatpointId)&get_treatpoints").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                   /* var treatpointsResults: [String] = []
                    for treatpoint in treatpoints {
                        let value:String = treatpoint["treatpoint_id"] as! String
                        let name: String = treatpoint["treatpoint_name"] as! String
                        
                        treatpointsResults.append(name)
                        strongSelf.treatpoints[treatpoint["treatpoint_name"] as! String] = value
                    } */
                    SVProgressHUD.dismiss()
                    //strongSelf.treatpointList.optionArray = treatpointsResults
                    //                    strongSelf.treatPointList.optionIds = treatpointsIds
                   // strongSelf.treatpointList.selectedIndex = 0
                    strongSelf.treatpointId = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.treatpointLabel.text = " "+String(treatpoints[0]["treatpoint_name"] as! String)+" "
                   // strongSelf.treatpointList.text = treatpointsResults[0]
                    
                }
            }
        }

        
        
    }
    
    @IBAction func check(_ sender: Any) {
        let checkbox = sender as! Checkbox
        print(checkbox.tag)
    }
    
    @objc func commentsDidChange(_ textField: UITextField) {
        self.commentsValue = textField.text!
    }    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
        
        if (self.dateValue != "" && self.treatpointId != "" ) {
            self.saveButton.isEnabled = true
            self.saveButton.alpha = 1
        } else {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.5
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
//        self.present(ScannerViewController(clientId,userId,"monitoring"), animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        
        SVProgressHUD.show()
        final_comment_value = commentsTextfield.text!
        
        let EntryUrl = "\(baseURL)MonitoringPlan?save_monitoring_value=1&machine_id=\(machineId)&user_id=\(userId)&treatpoint_id=\(treatpointId)&start_date=\(dateValue)&comments=\(final_comment_value)&ph_factor=\(phFactorCheck.isChecked)&iron_total=\(ironTotalCheck.isChecked)&total_hardness=\(totalHardnessCheck.isChecked)&oxygen=\(oxygenCheck.isChecked)&hydrogen_sulfide=\(hydrogenCheck.isChecked)&phosophonate=\(phosophonateCheck.isChecked)&total_solids=\(totalSolidsCheck.isChecked)&formic_acid=\(formicAcidCheck.isChecked)&acetic_acid=\(aceticAcidCheck.isChecked)&propionic_acid=\(propionicAcidCheck.isChecked)&butyric_acid=\(buryticAcidCheck.isChecked)&valeric_acid=\(valericAcidCheck.isChecked)&manganese=\(manganeseCheck.isChecked)&silica=\(silicaCheck.isChecked)&cid=\(cidCheck.isChecked)&coupon=\(couponCheck.isChecked)&lpr=\(lprCheck.isChecked)&er=\(erCheck.isChecked)&microcorr=\(microcorrCheck.isChecked)&potentiadyne=\(potentiadymeCheck.isChecked)&amine_residual=\(amineResidualCheck.isChecked)&srb=\(srbCheck.isChecked)&apb=\(apbCheck.isChecked)&atp=\(atpCheck.isChecked)&oil_in_water=\(oilCheck.isChecked)&turbidity=\(turbidityCheck.isChecked)&carbon_dioxide=\(carbonDioxideCheck.isChecked)&sulfate=\(sulfateCheck.isChecked)&gab=\(gabCheck.isChecked)&bicarbonate=\(bicarbonateCheck.isChecked)&last_updated_by=\(clientId)"
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        Alamofire.request(encodedUrl!).responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}
                        
                        //------------------ Gaurav Code --------------------
                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                        }
                        else{
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1

                        switch val {
                        case "form_4":
                            let vc = AnalysisViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            break
                        case "form_5":
                            let vc = AnalysisViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                            
                        case "form_6":
                            let vc = SamplingViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_7":
                            let vc = WellTestViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            break
                        case "form_8":
                            let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_9":
                            let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_10":
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                            vc.client_id = strongSelf.clientId
                            vc.user_id = strongSelf.userId
                            vc.arr_ViewController = strongSelf.arr_ViewController!
                            strongSelf.present(vc, animated: true, completion: nil)
                        default:
                            break
                        }
                            }
                            
                        }
                        //------------------ End --------------------
                        
//                        strongSelf.present(DashboardViewController(strongSelf.clientId, strongSelf.userId), animated: true, completion: nil)
                        
                        
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
}

extension MonitoringPlanViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == treatpointList || textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
