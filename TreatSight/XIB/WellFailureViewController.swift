//
//  WellFailureViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 16/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import iOSDropDown
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD

class WellFailureViewController: UIViewController{
    
    @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var failureList: DropDown!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var commentsTextfield: UITextView!
    @IBOutlet weak var saveButton: UIButton!
  
    var failures =  Dictionary<String, String>()
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    var treatpointId: String
    var userId: String
    var clientId: String
    var failureId: String = ""
    var dateValue: String!
    var commentsValue: String = ""
    var final_comment_value: String = ""

    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""

    @IBOutlet weak var dropdown_comment: DropDown!

    
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        
        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment
        
        strComment = ""
        commentsTextfield.layer.borderColor = UIColor.darkGray.cgColor
        commentsTextfield.layer.borderWidth = 1.0

    }
    
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        commentsTextfield.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        strComment = ""
        strComment = commentsTextfield.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        commentsTextfield.text  = strComment
        final_comment_value = commentsTextfield.text!
    }
    
    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.machineId = machineId
        self.treatpointId = tpId
        super.init(nibName: nil, bundle: nil)
        self.arr_ViewController = arrViewM

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
//        saveButton.alpha = 0.5
//        saveButton.isEnabled = false
        
//        commentsTextfield.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        commentsTextfield.inputAccessoryView = getToolbar()

        failureList.delegate = self
        failureList.selectedRowColor = themeColor()

        failureList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.failureId = strongSelf.failures[string]!
            
        }
        dropdown_comment.selectedRowColor = themeColor()

        Alamofire.request("\(baseURL)WellFailures?get_wellfailure_categories=1").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux :[String: Any] = json["data"] as! [String: Any]
                    let failures :[[String: String]] = aux["failureCategories"] as! [[String: String]]
                    var treatpointsResults: [String] = []
                    for failure in failures {
                        treatpointsResults.append(failure["failure_category_name"]!)
                        strongSelf.failures[failure["failure_category_name"]!] = failure["failure_category_id"]
                    }
                    strongSelf.failureList.optionArray = treatpointsResults
                    strongSelf.failureList.selectedIndex = 0
                    strongSelf.failureId = failures[0]["failure_category_id"]!
                    strongSelf.failureList.text = treatpointsResults[0]

                }
            }
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.dateValue = myString

    }
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
        
        if (self.dateValue != "" && self.failureId != "" ) {
            self.saveButton.isEnabled = true
            self.saveButton.alpha = 1
        } else {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.5
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
//        self.present(ScannerViewController(clientId,userId,"well-failure"), animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {

        SVProgressHUD.show()
        final_comment_value = commentsTextfield.text!
        let EntryUrl = "\(baseURL)WellFailures?save_wellfailure_form_value=1&machine_id=\(machineId)&failure_category_id=\(failureId)&failure_date=\(dateValue!)&user_id=\(userId)&last_updated_by=\(clientId)&comments=\(final_comment_value)"
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        
        Alamofire.request(encodedUrl!).responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}
                        
                        //------------------ Gaurav Code --------------------
                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                            
                        }
                        else{
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1

                        switch val {
                        case "form_6":
                            let vc = SamplingViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_7":
                            let vc = WellTestViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            break
                        case "form_8":
                            let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_9":
                            let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_10":
//                            let vc = Questions_VC(strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
//                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                            vc.client_id = strongSelf.clientId
                            vc.user_id = strongSelf.userId
                            vc.arr_ViewController = strongSelf.arr_ViewController!
                            strongSelf.present(vc, animated: true, completion: nil)

                            break

                        default:
                            break
                            }//Switch
                        }// If Let
                    }//else
                        //------------------ End --------------------
                        
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
    
    @objc func commentsDidChange(_ textField: UITextField) {
        self.commentsValue = textField.text!
    }
    
}

extension WellFailureViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == failureList || textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
