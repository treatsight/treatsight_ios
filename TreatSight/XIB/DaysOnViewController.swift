//
//  DaysOnViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 17/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import iOSDropDown
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD



class DaysOnViewController: UIViewController{
    
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    var treatpointId: String
    var userId: String
    var clientId: String
    var month: Int = 0
    var year: Int = 0
    var producing: Float = 0
    var injecting: Float = 0
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""

    
    @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var monthTextfield: UITextField!
    @IBOutlet weak var yearTextfield: UITextField!
    @IBOutlet weak var producingTextfield: UITextField!
    @IBOutlet weak var injectingTextfield: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    var final_comment_value: String = ""

    @IBAction func logoutAction(_ sender: Any) {

        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        
//
//        dropdown_comment.selectedRowColor = themeColor()
//
//        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
//            if let json = response.result.value as? [String: Any]{
//                if (json["response"] as! NSString).intValue == 1{
//                    guard let strongSelf = self else {return}
//                    var productsResults: [String] = []
//                    let aux = json["data"] as! [[String: Any]]
//                    let products = aux as! [[String: String]]
//                    //entry_value
//                    for product in products {
//                        productsResults.append(product["entry_description"]!)
//                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
//                    }
//                    strongSelf.dropdown_comment.optionArray = productsResults
//                    //strongSelf.productList.optionIds = productsIds
//                    strongSelf.dropdown_comment.selectedIndex = 0
//                    strongSelf.comment_value = products[0]["entry_value"]!
//                    strongSelf.dropdown_comment.text = productsResults[0]
//
//                }
//            }
//        }//End Of Comment

    }
    

    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.treatpointId = tpId
        self.machineId = machineId
        super.init(nibName: nil, bundle: nil)
        self.arr_ViewController = arrViewM

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
        
        monthTextfield.addTarget(self, action: #selector(DaysOnViewController.monthDidChange(_:)), for: UIControl.Event.editingChanged)
        monthTextfield.inputAccessoryView = getToolbar()
        yearTextfield.addTarget(self, action: #selector(DaysOnViewController.yearDidChange(_:)), for: UIControl.Event.editingChanged)
        yearTextfield.inputAccessoryView = getToolbar()
        producingTextfield.addTarget(self, action: #selector(DaysOnViewController.producingDidChange(_:)), for: UIControl.Event.editingChanged)
        producingTextfield.inputAccessoryView = getToolbar()
        injectingTextfield.addTarget(self, action: #selector(DaysOnViewController.injectingDidChange(_:)), for: UIControl.Event.editingChanged)
        injectingTextfield.inputAccessoryView = getToolbar()
    }
    
    
    
    private func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func monthDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){
            self.month = Int(textField.text!)!
        
           // if (self.month > 0 && self.month <= 12) {
                saveButton.alpha = 1
                saveButton.isEnabled = true
           // } else {
           //     saveButton.alpha = 0.5
           //     saveButton.isEnabled = false
           // }
        }
    }
    
    @objc func yearDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

           // if (self.month != 0 && self.year != 0 && (self.year > 1900 && self.year < 2100) ) {
                saveButton.alpha = 1
                saveButton.isEnabled = true
           // } else {
           //     saveButton.alpha = 0.5
           //     saveButton.isEnabled = false
           // }
        }
    }
    
    @objc func producingDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){
        self.producing = Float(textField.text!)!
        }
    }
    
    @objc func injectingDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){
        self.injecting = Float(textField.text!)!
        }
    }
    
    @IBAction func goBAck(_ sender: Any) {
//        self.present(ScannerViewController(clientId,userId,"days-on"), animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        SVProgressHUD.show()

        Alamofire.request("\(baseURL)WellDaysOn?&treatsite_id==\(machineId)&save_welldayson_form_value=1&machine_id=\(machineId)&user_id=\(userId)&report_month=\(month)&report_year=\(year)&days_on_prod=\(producing)&days_on_inj=\(injecting)&last_updated_by=\(clientId)").responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        
                        //------------------ Gaurav Code --------------------
                        
                        
                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                            
                        }
                        else{
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1

                                switch val {
                            case "form_9":
                                let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                vc.modalPresentationStyle = .fullScreen
                                strongSelf.present(vc, animated: true, completion: nil)
                                break
                            case "form_10":
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                                vc.client_id = strongSelf.clientId
                                vc.user_id = strongSelf.userId
                                vc.arr_ViewController = strongSelf.arr_ViewController!
                                strongSelf.present(vc, animated: true, completion: nil)

                                break

                            default:
                                break
                                }//Switch
                            }// If Let
                        }//else
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)

             
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
}

