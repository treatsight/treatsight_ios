//
//  SamplingViewController.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 03/05/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField
import Alamofire
import MBProgressHUD
import SVProgressHUD


class SamplingViewController: UIViewController {
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    var treatpointId: String
    var clientId: String
    var userId: String
    var dateValue: String = ""
    var commentsValue: String = ""
    var bottleLabelValue: String = ""
    var preValue: Float = 0
    var volumeValue: Float = 0
    var postValue: Float = 0
    var productId: String = ""
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""
    var final_comment_value: String = ""

    
    var products =  Dictionary<String, String>()
    @IBOutlet weak var productList: DropDown!
    @IBOutlet weak var txtFeild_BottleLabel: SkyFloatingLabelTextField!
    @IBAction func logoutAction(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "user_name")
        UserDefaults.standard.removeObject(forKey: "password")
        UserDefaults.standard.removeObject(forKey: "isRemembered")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    @IBOutlet weak var dropdown_comment: DropDown!
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        txtFeild_Comments.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        
        strComment = ""
        strComment = txtFeild_Comments.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        txtFeild_Comments.text  = strComment
        final_comment_value = txtFeild_Comments.text!
        
    }
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

 
    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_pageCount.text = strCount
        
        dropdown_comment.selectedRowColor = themeColor()

        if arrayCount == counterOfViewController {
            btn_save.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }

        txtFeild_BottleLabel.addTarget(self, action: Selector(("bottleLabelDidChange:")), for: UIControl.Event.editingChanged)
        txtFeild_BottleLabel.inputAccessoryView = getToolbar()
        
//        txtFeild_Comments.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        txtFeild_Comments.inputAccessoryView = getToolbar()

        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment
        strComment = ""
        txtFeild_Comments.layer.borderColor = UIColor.darkGray.cgColor
        txtFeild_Comments.layer.borderWidth = 1.0


    }
    
    
    @objc func bottleLabelDidChange(_ textField: UITextField) {
        self.bottleLabelValue = textField.text!
    }

    
    @objc func commentsDidChange(_ textField: UITextField) {
        self.commentsValue = textField.text!
    }

    func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }

    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.machineId = machineId
        self.treatpointId = tpId
        self.arr_ViewController = arrViewM
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let strDate = formatter.string(from: datePicker.date)
        self.dateValue = strDate
    }
    
    
    @IBOutlet weak var lbl_pageCount: UILabel!
    
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var txtFeild_Comments: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var btn_save: UIButton!
    
    
  
    
    @IBAction func logout_action(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    @IBAction func home_Action(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    

 
    @IBAction func save_Action(_ sender: Any) {
        
        self.bottleLabelValue = txtFeild_BottleLabel.text!
        final_comment_value = txtFeild_Comments.text!

        SVProgressHUD.show()
        let EntryUrl = "\(baseURL)SamplingForms?sampling_date=\(String(describing: dateValue))&sampling_bottle_labeling=\(bottleLabelValue)&comments=\(final_comment_value)&treatsite_id=\(machineId)&last_updated_by=\(userId)&save_form_value=s&machine_id=\(machineId)&client_id=\(clientId)"
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        
        Alamofire.request(encodedUrl!).responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}
                        //------------------ Gaurav Code --------------------
                        if self!.btn_save.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                        }
                        else{
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){

                                counterOfViewController = counterOfViewController + 1

                        switch val {
                        case "form_7":
                            let vc = WellTestViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            
                            break
                        case "form_8":
                            let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_9":
                            let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                            vc.modalPresentationStyle = .fullScreen
                            strongSelf.present(vc, animated: true, completion: nil)
                            break
                        case "form_10":
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                            vc.client_id = strongSelf.clientId
                            vc.user_id = strongSelf.userId
                            vc.arr_ViewController = strongSelf.arr_ViewController!
                            strongSelf.present(vc, animated: true, completion: nil)
                            break

                        default:
                            break
                        }//Switch
                    }// If Let
                }//else
                        //------------------ End --------------------
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()

                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }

    /////////////////////////////
    @IBAction func dateChanged(_ sender: Any) {
        ///////////////////////////////
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd"
        
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
        print(myString)
        print(dateValue)
    }
    
    
    


}

extension SamplingViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if ( textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
