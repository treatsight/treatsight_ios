//
//  ChildLabel_Cell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 30/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import UIKit

class ChildLabel_Cell: UITableViewCell {

    @IBOutlet weak var lbl_value: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
