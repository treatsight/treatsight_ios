//
//  ChildChecked_Cell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 30/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import UIKit
//1. delegate method
protocol Child_Checked_Cell_Delegate: AnyObject {
    func childCheckedTapped(cell: ChildChecked_Cell)

}
class ChildChecked_Cell: UITableViewCell {

    
    @IBOutlet weak var btn_checked: UIButton!
    @IBOutlet weak var lbl_value: UILabel!
    weak var delegate: Child_Checked_Cell_Delegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func checkedClicked(_ sender: Any) {
        delegate?.childCheckedTapped(cell: self)
    }
}
