//
//  ChildRadio_Cell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 30/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import UIKit
//1. delegate method
protocol Child_Radio_Cell_Delegate: AnyObject {
    func childRadioTapped(cell: ChildRadio_Cell)

}

class ChildRadio_Cell: UITableViewCell {

    @IBOutlet weak var btn_radio: UIButton!
    @IBOutlet weak var lbl_value: UILabel!
    weak var delegate: Child_Radio_Cell_Delegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func childRadioClicked(_ sender: Any) {
         delegate?.childRadioTapped(cell: self)
     }
    
}
