//
//  Radio_Cell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 18/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import UIKit

//1. delegate method
protocol Radio_Cell_Delegate: AnyObject {
    func radioTapped(cell: Radio_Cell)

}

class Radio_Cell: UITableViewCell {

    @IBOutlet weak var btn_radio: UIButton!
    @IBOutlet weak var lbl_value: UILabel!
    weak var delegate: Radio_Cell_Delegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func radioClicked(_ sender: Any) {
        delegate?.radioTapped(cell: self)
    }
    
    
}
