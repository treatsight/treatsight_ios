//
//  Checkbox_Cell.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 16/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import UIKit
import SimpleCheckbox


//1. delegate method
protocol CheckBox_Cell_Delegate: AnyObject {
    func checkedTapped(cell: Checkbox_Cell)

}
class Checkbox_Cell: UITableViewCell {

    @IBOutlet weak var btn_checkBox: UIButton!{
        didSet{
            btn_checkBox.imageView?.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var checkBox: Checkbox!
    @IBOutlet weak var lbl_value: UILabel!
    weak var delegate: CheckBox_Cell_Delegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func checkedClicked(_ sender: Any) {
        delegate?.checkedTapped(cell: self)
    }
}
