//
//  AnalysisViewController.swift
//  TreatSight
//
//  Created by rodrigo camparo on 18/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown
import Alamofire
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD


class AnalysisViewController: UIViewController {
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineId: String
    //var treatpoint_Id: String
    var userId: String
    var clientId: String
    var dateValue: String = ""
    
    var comments_array =  Dictionary<String, String>()
    var comment_value: String = ""
    var comment_description: String = ""
    
    
    var analysisTypes =  Dictionary<String, String>()
    var treatpoints =  Dictionary<String, String>()
    
    @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var treatpointList: DropDown!
    var treatpointId: String = ""
    @IBOutlet weak var analysisList: DropDown!
    var analysisId: String = ""
    @IBOutlet weak var commentsTF: UITextView!
    var comments: String = ""
    @IBOutlet weak var phTF: UITextField!
    var ph: Float = 0
    @IBOutlet weak var ironTotalTF: UITextField!
    var ironTotal: Float = 0
    @IBOutlet weak var ironSolubleTF: UITextField!
    var ironSoluble: Float = 0
    @IBOutlet weak var alkalinityTF: UITextField!
    var alkalinity: Float = 0
    @IBOutlet weak var totalHardnessTF: UITextField!
    var totalHardness: Float = 0
    @IBOutlet weak var oxygenTF: UITextField!
    var oxygen: Float = 0
    @IBOutlet weak var hydrogenSulfideTF: UITextField!
    var hydrogenSulfide: Float = 0
    @IBOutlet weak var phosophonateTF: UITextField!
    var phosophonate: Float = 0
    @IBOutlet weak var bariumTF: UITextField!
    var barium: Float = 0
    @IBOutlet weak var strontiumTF: UITextField!
    var strontium: Float = 0
    @IBOutlet weak var totalSolidsTF: UITextField!
    var totalSolids: Float = 0
    @IBOutlet weak var sampleVolumeTF: UITextField!
    var sampleVolume: Float = 0
    @IBOutlet weak var hydrocarbonCompoundsTF: UITextField!
    var hydrocarbonCompounds: Float = 0
    @IBOutlet weak var acidSolubleTF: UITextField!
    var acidSoluble: Float = 0
    @IBOutlet weak var formicAcidTF: UITextField!
    @IBOutlet weak var acidInsolubleTF: UITextField!
    var acidInsoluble: Float = 0
    var formicAcid: Float = 0
    @IBOutlet weak var aceticAcidTF: UITextField!
    var aceticAcid: Float = 0
    @IBOutlet weak var propionicAcidTF: UITextField!
    var propionicAcid: Float = 0
    @IBOutlet weak var butyricAcidTF: UITextField!
    var butyricAcid: Float = 0
    @IBOutlet weak var valericAcidTF: UITextField!
    var valericAcid: Float = 0
    @IBOutlet weak var manganeseTF: UITextField!
    var manganese: Float = 0
    @IBOutlet weak var calciumTF: UITextField!
    var calcium: Float = 0
    @IBOutlet weak var magnesiumTF: UITextField!
    var magnesium: Float = 0
    @IBOutlet weak var silicaTF: UITextField!
    var silica: Float = 0
    @IBOutlet weak var chloridesTF: UITextField!
    var chlorides: Float = 0
    @IBOutlet weak var conductivityTF: UITextField!
    var conductivity: Float = 0
    @IBOutlet weak var cidTF: UITextField!
    var cid: Float = 0
    @IBOutlet weak var couponTF: UITextField!
    var coupon: Float = 0
    @IBOutlet weak var lprTF: UITextField!
    var lpr: Float = 0
    @IBOutlet weak var erTF: UITextField!
    var er: Float = 0
    @IBOutlet weak var microcorrTF: UITextField!
    var microcorr: Float = 0
    @IBOutlet weak var potentiadyneTF: UITextField!
    var potentiadyne: Float = 0
    @IBOutlet weak var amineResidualTF: UITextField!
    var amineResidual: Float = 0
    @IBOutlet weak var srbTF: UITextField!
    var srb: Float = 0
    @IBOutlet weak var apbTF: UITextField!
    var apb: Float = 0
    @IBOutlet weak var atpTF: UITextField!
    var atp: Int = 0
    @IBOutlet weak var oilTF: UITextField!
    var oil: Float = 0
    @IBOutlet weak var turbidityTF: UITextField!
    var turbidity: Float = 0
    @IBOutlet weak var carbonDioxideTF: UITextField!
    var carbonDioxide: Float = 0
    @IBOutlet weak var sulfateTF: UITextField!
    var sulfate: Float = 0
    @IBOutlet weak var zincTF: UITextField!
    var zinc: Float = 0
    @IBOutlet weak var leadTF: UITextField!
    var lead: Float = 0
    @IBOutlet weak var specificGravityTF: UITextField!
    var specificGravity: Float = 0
    @IBOutlet weak var linksTF: UITextField!
    var links: String = ""
    @IBOutlet weak var bicarbonateTF: UITextField!
    var bicarbonate: Float = 0
    @IBOutlet weak var daysTF: UITextField!
    var days: Int = 0
    @IBOutlet weak var gabTF: UITextField!
    var gab: Float = 0
    @IBOutlet weak var sodiumTF: UITextField!
    var sodium: Float = 0
    @IBOutlet weak var potassiumTF: UITextField!
    var potassium: Float = 0
    @IBOutlet weak var lithiumTF: UITextField!
    var lithium: Float = 0
    @IBOutlet weak var ammoniaTF: UITextField!
    var ammonia: Float = 0
    @IBOutlet weak var aluminumTF: UITextField!
    var aluminum: Float = 0
    @IBOutlet weak var boronTF: UITextField!
    var boron: Float = 0
    @IBOutlet weak var bromineTF: UITextField!
    var bromine: Float = 0
    @IBOutlet weak var phosphateTF: UITextField!
    var phosphate: Float = 0
    @IBOutlet weak var nitrateTF: UITextField!
    var nitrate: Float = 0
    @IBOutlet weak var florideTF: UITextField!
    var floride: Float = 0
    @IBOutlet weak var carbonateTF: UITextField!
    var carbonate: Float = 0
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dropdown_comment: DropDown!
    var final_comment_value: String = ""
    
   
    @IBOutlet weak var treatpointLabel: UILabel!
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        dropdown_comment.selectedRowColor = themeColor()
        
        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                    
                }
            }
        }//End Of Comment
        
        strComment = ""
        commentsTF.layer.borderColor = UIColor.darkGray.cgColor
        commentsTF.layer.borderWidth = 1.0
        
    }
    
    
    
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
        commentsTF.text  = strComment
        final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        
        strComment = ""
        strComment = commentsTF.text! + " " + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        print(strComment)
        commentsTF.text  = strComment
        final_comment_value = commentsTF.text!
        
    }
    
    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.clientId = client_id
        self.userId = user
        self.treatpointId = tpId
        self.machineId = machineId
        super.init(nibName: nil, bundle: nil)
        self.arr_ViewController = arrViewM
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.treatpointLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.treatpointLabel.layer.borderWidth = 1.0
        self.treatpointLabel.layer.cornerRadius = 5.0
        
        //        commentsTF.addTarget(self, action: "commentsDidChange:", for: UIControl.Event.editingChanged)
        commentsTF.inputAccessoryView = getToolbar()
        phTF.addTarget(self, action: #selector(AnalysisViewController.phDidChange(_:)), for: UIControl.Event.editingChanged)
        phTF.inputAccessoryView = getToolbar()
        ironTotalTF.addTarget(self, action: #selector(AnalysisViewController.ironTotalDidChange(_:)), for: UIControl.Event.editingChanged)
        ironTotalTF.inputAccessoryView = getToolbar()
        ironSolubleTF.addTarget(self, action: #selector(AnalysisViewController.ironSolubleDidChange(_:)), for: UIControl.Event.editingChanged)
        ironSolubleTF.inputAccessoryView = getToolbar()
        alkalinityTF.addTarget(self, action: #selector(AnalysisViewController.alkalinityDidChange(_:)), for: UIControl.Event.editingChanged)
        alkalinityTF.inputAccessoryView = getToolbar()
        totalHardnessTF.addTarget(self, action: #selector(AnalysisViewController.totalHardnessDidChange(_:)), for: UIControl.Event.editingChanged)
        totalHardnessTF.inputAccessoryView = getToolbar()
        oxygenTF.addTarget(self, action: #selector(AnalysisViewController.oxygenDidChange(_:)), for: UIControl.Event.editingChanged)
        oxygenTF.inputAccessoryView = getToolbar()
        hydrogenSulfideTF.addTarget(self, action: #selector(AnalysisViewController.hydrogenSulfideDidChange(_:)), for: UIControl.Event.editingChanged)
        hydrogenSulfideTF.inputAccessoryView = getToolbar()
        phosophonateTF.addTarget(self, action: #selector(AnalysisViewController.phosophonateDidChange(_:)), for: UIControl.Event.editingChanged)
        phosophonateTF.inputAccessoryView = getToolbar()
        bariumTF.addTarget(self, action: #selector(AnalysisViewController.bariumDidChange(_:)), for: UIControl.Event.editingChanged)
        bariumTF.inputAccessoryView = getToolbar()
        strontiumTF.addTarget(self, action: #selector(AnalysisViewController.strontiumDidChange(_:)), for: UIControl.Event.editingChanged)
        strontiumTF.inputAccessoryView = getToolbar()
        totalSolidsTF.addTarget(self, action: #selector(AnalysisViewController.totalSolidsDidChange(_:)), for: UIControl.Event.editingChanged)
        totalSolidsTF.inputAccessoryView = getToolbar()
        sampleVolumeTF.addTarget(self, action: #selector(AnalysisViewController.sampleVolumeDidChange(_:)), for: UIControl.Event.editingChanged)
        sampleVolumeTF.inputAccessoryView = getToolbar()
        hydrocarbonCompoundsTF.addTarget(self, action: #selector(AnalysisViewController.hydrocarbonCompoundsDidChange(_:)), for: UIControl.Event.editingChanged)
        hydrocarbonCompoundsTF.inputAccessoryView = getToolbar()
        acidSolubleTF.addTarget(self, action: #selector(AnalysisViewController.acidSolubleDidChange(_:)), for: UIControl.Event.editingChanged)
        acidSolubleTF.inputAccessoryView = getToolbar()
        acidInsolubleTF.addTarget(self, action: #selector(AnalysisViewController.acidInsolubleDidChange(_:)), for: UIControl.Event.editingChanged)
        acidInsolubleTF.inputAccessoryView = getToolbar()
        formicAcidTF.addTarget(self, action: #selector(AnalysisViewController.formicAcidDidChange(_:)), for: UIControl.Event.editingChanged)
        formicAcidTF.inputAccessoryView = getToolbar()
        aceticAcidTF.addTarget(self, action: #selector(AnalysisViewController.aceticAcidDidChange(_:)), for: UIControl.Event.editingChanged)
        aceticAcidTF.inputAccessoryView = getToolbar()
        propionicAcidTF.addTarget(self, action: #selector(AnalysisViewController.propionicAcidDidChange(_:)), for: UIControl.Event.editingChanged)
        propionicAcidTF.inputAccessoryView = getToolbar()
        butyricAcidTF.addTarget(self, action: #selector(AnalysisViewController.butyricAcidDidChange(_:)), for: UIControl.Event.editingChanged)
        butyricAcidTF.inputAccessoryView = getToolbar()
        valericAcidTF.addTarget(self, action: #selector(AnalysisViewController.valericAcidDidChange(_:)), for: UIControl.Event.editingChanged)
        valericAcidTF.inputAccessoryView = getToolbar()
        manganeseTF.addTarget(self, action: #selector(AnalysisViewController.manganeseDidChange(_:)), for: UIControl.Event.editingChanged)
        manganeseTF.inputAccessoryView = getToolbar()
        calciumTF.addTarget(self, action: #selector(AnalysisViewController.calciumDidChange(_:)), for: UIControl.Event.editingChanged)
        calciumTF.inputAccessoryView = getToolbar()
        magnesiumTF.addTarget(self, action: #selector(AnalysisViewController.magnesiumDidChange(_:)), for: UIControl.Event.editingChanged)
        magnesiumTF.inputAccessoryView = getToolbar()
        silicaTF.addTarget(self, action: #selector(AnalysisViewController.silicaDidChange(_:)), for: UIControl.Event.editingChanged)
        silicaTF.inputAccessoryView = getToolbar()
        chloridesTF.addTarget(self, action: #selector(AnalysisViewController.chloridesDidChange(_:)), for: UIControl.Event.editingChanged)
        chloridesTF.inputAccessoryView = getToolbar()
        conductivityTF.addTarget(self, action: #selector(AnalysisViewController.conductivityDidChange(_:)), for: UIControl.Event.editingChanged)
        conductivityTF.inputAccessoryView = getToolbar()
        cidTF.addTarget(self, action: #selector(AnalysisViewController.cidDidChange(_:)), for: UIControl.Event.editingChanged)
        cidTF.inputAccessoryView = getToolbar()
        couponTF.addTarget(self, action: #selector(AnalysisViewController.couponDidChange(_:)), for: UIControl.Event.editingChanged)
        couponTF.inputAccessoryView = getToolbar()
        lprTF.addTarget(self, action: #selector(AnalysisViewController.lprDidChange(_:)), for: UIControl.Event.editingChanged)
        lprTF.inputAccessoryView = getToolbar()
        erTF.addTarget(self, action: #selector(AnalysisViewController.erDidChange(_:)), for: UIControl.Event.editingChanged)
        erTF.inputAccessoryView = getToolbar()
        microcorrTF.addTarget(self, action: #selector(AnalysisViewController.microcorrDidChange(_:)), for: UIControl.Event.editingChanged)
        microcorrTF.inputAccessoryView = getToolbar()
        potentiadyneTF.addTarget(self, action: #selector(AnalysisViewController.potentiadyneDidChange(_:)), for: UIControl.Event.editingChanged)
        potentiadyneTF.inputAccessoryView = getToolbar()
        amineResidualTF.addTarget(self, action: #selector(AnalysisViewController.amineResidualDidChange(_:)), for: UIControl.Event.editingChanged)
        amineResidualTF.inputAccessoryView = getToolbar()
        srbTF.addTarget(self, action: #selector(AnalysisViewController.srbDidChange(_:)), for: UIControl.Event.editingChanged)
        srbTF.inputAccessoryView = getToolbar()
        apbTF.addTarget(self, action: #selector(AnalysisViewController.apbDidChange(_:)), for: UIControl.Event.editingChanged)
        apbTF.inputAccessoryView = getToolbar()
        atpTF.addTarget(self, action: #selector(AnalysisViewController.atpDidChange(_:)), for: UIControl.Event.editingChanged)
        atpTF.inputAccessoryView = getToolbar()
        oilTF.addTarget(self, action: #selector(AnalysisViewController.oilDidChange(_:)), for: UIControl.Event.editingChanged)
        oilTF.inputAccessoryView = getToolbar()
        turbidityTF.addTarget(self, action: #selector(AnalysisViewController.turbidityDidChange(_:)), for: UIControl.Event.editingChanged)
        turbidityTF.inputAccessoryView = getToolbar()
        carbonDioxideTF.addTarget(self, action: #selector(AnalysisViewController.carbonDioxideDidChange(_:)), for: UIControl.Event.editingChanged)
        carbonDioxideTF.inputAccessoryView = getToolbar()
        sulfateTF.addTarget(self, action: #selector(AnalysisViewController.sulfateDidChange(_:)), for: UIControl.Event.editingChanged)
        sulfateTF.inputAccessoryView = getToolbar()
        zincTF.addTarget(self, action: #selector(AnalysisViewController.zincDidChange(_:)), for: UIControl.Event.editingChanged)
        zincTF.inputAccessoryView = getToolbar()
        leadTF.addTarget(self, action: #selector(AnalysisViewController.leadDidChange(_:)), for: UIControl.Event.editingChanged)
        leadTF.inputAccessoryView = getToolbar()
        specificGravityTF.addTarget(self, action: #selector(AnalysisViewController.specificGravityDidChange(_:)), for: UIControl.Event.editingChanged)
        specificGravityTF.inputAccessoryView = getToolbar()
        linksTF.addTarget(self, action: #selector(AnalysisViewController.linksDidChange(_:)), for: UIControl.Event.editingChanged)
        linksTF.inputAccessoryView = getToolbar()
        bicarbonateTF.addTarget(self, action: #selector(AnalysisViewController.bicarbonateDidChange(_:)), for: UIControl.Event.editingChanged)
        bicarbonateTF.inputAccessoryView = getToolbar()
        daysTF.addTarget(self, action: #selector(AnalysisViewController.daysDidChange(_:)), for: UIControl.Event.editingChanged)
        daysTF.inputAccessoryView = getToolbar()
        gabTF.addTarget(self, action: #selector(AnalysisViewController.gabDidChange(_:)), for: UIControl.Event.editingChanged)
        gabTF.inputAccessoryView = getToolbar()
        sodiumTF.addTarget(self, action: #selector(AnalysisViewController.sodiumDidChange(_:)), for: UIControl.Event.editingChanged)
        sodiumTF.inputAccessoryView = getToolbar()
        potassiumTF.addTarget(self, action: #selector(AnalysisViewController.potassiumDidChange(_:)), for: UIControl.Event.editingChanged)
        potassiumTF.inputAccessoryView = getToolbar()
        lithiumTF.addTarget(self, action: #selector(AnalysisViewController.lithiumDidChange(_:)), for: UIControl.Event.editingChanged)
        lithiumTF.inputAccessoryView = getToolbar()
        ammoniaTF.addTarget(self, action: #selector(AnalysisViewController.ammoniaDidChange(_:)), for: UIControl.Event.editingChanged)
        ammoniaTF.inputAccessoryView = getToolbar()
        aluminumTF.addTarget(self, action: #selector(AnalysisViewController.aluminumDidChange(_:)), for: UIControl.Event.editingChanged)
        aluminumTF.inputAccessoryView = getToolbar()
        boronTF.addTarget(self, action: #selector(AnalysisViewController.boronDidChange(_:)), for: UIControl.Event.editingChanged)
        boronTF.inputAccessoryView = getToolbar()
        bromineTF.addTarget(self, action: #selector(AnalysisViewController.bromineDidChange(_:)), for: UIControl.Event.editingChanged)
        bromineTF.inputAccessoryView = getToolbar()
        phosphateTF.addTarget(self, action: #selector(AnalysisViewController.phosphateDidChange(_:)), for: UIControl.Event.editingChanged)
        phosphateTF.inputAccessoryView = getToolbar()
        nitrateTF.addTarget(self, action: #selector(AnalysisViewController.nitrateDidChange(_:)), for: UIControl.Event.editingChanged)
        nitrateTF.inputAccessoryView = getToolbar()
        florideTF.addTarget(self, action: #selector(AnalysisViewController.florideDidChange(_:)), for: UIControl.Event.editingChanged)
        florideTF.inputAccessoryView = getToolbar()
        carbonateTF.addTarget(self, action: #selector(AnalysisViewController.carbonateDidChange(_:)), for: UIControl.Event.editingChanged)
        carbonateTF.inputAccessoryView = getToolbar()
        
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
       /*
        treatpointList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.treatpointId = strongSelf.treatpoints[string]!
        }
        */
        analysisList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.analysisId = strongSelf.analysisTypes[string]!
            
            if (strongSelf.analysisId != "" && strongSelf.dateValue != "" && strongSelf.treatpointId != "" ) {
                strongSelf.saveButton.isEnabled = true
                strongSelf.saveButton.alpha = 1
            } else {
                strongSelf.saveButton.isEnabled = false
                strongSelf.saveButton.alpha = 0.5
            }
        }
        
      /*  treatpointList.delegate = self
        treatpointList.selectedRowColor = themeColor()
        */
        analysisList.delegate = self
        analysisList.selectedRowColor = themeColor()
        
        
        SVProgressHUD.show()
        Alamofire.request("\(baseURL)ctm?user_id=\(userId)&client_id=\(clientId)&machineid=\(treatpointId)&get_treatpoints").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                    /* var treatpointsResults: [String] = []
                    for treatpoint in treatpoints {
                        let value:String = treatpoint["treatpoint_id"] as! String
                        let name: String = treatpoint["treatpoint_name"] as! String
                        
                        
                        treatpointsResults.append(name)
                        strongSelf.treatpoints[treatpoint["treatpoint_name"] as! String] = value
                    } */
                    SVProgressHUD.dismiss()
                    //strongSelf.treatpointList.optionArray = treatpointsResults
                    //strongSelf.treatPointList.optionIds = treatpointsIds
                    //strongSelf.treatpointList.selectedIndex = 0
                    strongSelf.treatpointId = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.treatpointLabel.text = " "+String(treatpoints[0]["treatpoint_name"] as! String)+" "
                   // strongSelf.treatpointList.text = treatpointsResults[0]
                    
                }
            }
        }
        
        
        
        Alamofire.request("\(baseURL)analysis?get_analysis_type=1").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                    
                    var typesResults: [Any] = []
                    for type in treatpoints {
                        let val:Int = type["analysis_type"]! as! Int
                        let value:String = String(describing: val)
                        let name: String = type["analysis_name"] as! String
                        typesResults.append(name)
                        strongSelf.analysisTypes[type["analysis_name"] as! String] = value
                        
                    }
                    
                    strongSelf.analysisList.optionArray = typesResults as! [String]
                    strongSelf.analysisList.selectedIndex = 0
                    let str: Int = treatpoints[0]["analysis_type"] as! Int
                    strongSelf.analysisId = String(describing: str)
                    strongSelf.analysisList.text = typesResults[0] as? String
                    
                }
            }
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.dateValue = myString

        
        NotificationCenter.default.addObserver(self, selector: #selector(AnalysisViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AnalysisViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //1
    @objc func commentsDidChange(_ textField: UITextField) {
        self.comments = textField.text!
    }
    
    //2
    @objc func phDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.ph = 0;return}
        self.ph = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //3
    @objc func ironTotalDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.ironTotal = 0;return}
        self.ironTotal = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //4
    @objc func ironSolubleDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.ironSoluble = 0;return}
        self.ironSoluble = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //5
    @objc func alkalinityDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.alkalinity = 0;return}
        self.alkalinity = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //6
    @objc func totalHardnessDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.totalHardness = 0;return}
        self.totalHardness = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //7
    @objc func oxygenDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.oxygen = 0;return}
        self.oxygen = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //8
    @objc func hydrogenSulfideDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.hydrogenSulfide = 0;return}
        self.hydrogenSulfide = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //9
    @objc func phosophonateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.phosophonate = 0;return}
        self.phosophonate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //10
    @objc func bariumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.barium = 0;return}
        self.barium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //11
    @objc func strontiumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.strontium = 0;return}
        self.strontium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //12
    @objc func totalSolidsDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.totalSolids = 0;return}
        self.totalSolids = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //13
    @objc func sampleVolumeDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.sampleVolume = 0;return}
        self.sampleVolume = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //14
    @objc func hydrocarbonCompoundsDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.hydrocarbonCompounds = 0;return}
        self.hydrocarbonCompounds = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //15
    @objc func acidSolubleDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.acidSoluble = 0;return}
        self.acidSoluble = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //16
    @objc func acidInsolubleDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.acidInsoluble = 0;return}
        self.acidInsoluble = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //17
    @objc func formicAcidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.formicAcid = 0;return}
        self.formicAcid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //18
    @objc func aceticAcidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.aceticAcid = 0;return}
        self.aceticAcid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //19
    @objc func propionicAcidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.propionicAcid = 0;return}
        self.propionicAcid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //20
    @objc func butyricAcidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.butyricAcid = 0;return}
        self.butyricAcid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //21
    @objc func valericAcidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.valericAcid = 0;return}
        self.valericAcid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //22
    @objc func manganeseDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.manganese = 0;return}
        self.manganese = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //23
    @objc func calciumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.calcium = 0; return}
        self.calcium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //24
    @objc func magnesiumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.magnesium = 0;return}
        self.magnesium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //25
    @objc func silicaDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.silica = 0;return}
        self.silica = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //26
    @objc func chloridesDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.chlorides = 0;return}
        self.chlorides = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //27
    @objc func conductivityDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.conductivity = 0;return}
        self.conductivity = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //28
    @objc func cidDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.cid = 0;return}
        self.cid = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //29
    @objc func couponDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.coupon = 0;return}
        self.coupon = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //30
    @objc func lprDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.lpr = 0;return}
        self.lpr = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //31
    @objc func erDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.er = 0;return}
        self.er = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //32
    @objc func microcorrDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.microcorr = 0;return}
        self.microcorr = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //33
    @objc func potentiadyneDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.potentiadyne = 0;return}
        self.potentiadyne = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //34
    @objc func amineResidualDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.amineResidual = 0;return}
        self.amineResidual = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //35
    @objc func srbDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.srb = 0;return}
        self.srb = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //36
    @objc func apbDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.apb = 0;return}
        self.apb = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //37
    @objc func atpDidChange(_ textField: UITextField) {
        guard Int(textField.text!) != nil else {return}
        self.atp = Int(textField.text!)!
    }
    
    //38
    @objc func oilDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.oil = 0;return}
        self.oil = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //39
    @objc func turbidityDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.turbidity = 0;return}
        self.turbidity = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //40
    @objc func carbonDioxideDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.carbonDioxide = 0;return}
        self.carbonDioxide = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //41
    @objc func sulfateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.sulfate = 0;return}
        self.sulfate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //42
    @objc func zincDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.zinc = 0;return}
        self.zinc = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //43
    @objc func leadDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.lead = 0;return}
        self.lead = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //44
    @objc func specificGravityDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.specificGravity = 0;return}
        self.specificGravity = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //45
    @objc func linksDidChange(_ textField: UITextField) {
        self.links = textField.text!
    }
    
    //46
    @objc func bicarbonateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.bicarbonate = 0;return}
        self.bicarbonate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //47
    @objc func daysDidChange(_ textField: UITextField) {
        guard Int(textField.text!) != nil else {self.days = 0;return}
        self.days = Int(textField.text!)!
    }
    
    //48
    @objc func gabDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.gab = 0;return}
        self.gab = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //49
    @objc func sodiumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.sodium = 0;return}
        self.sodium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //50
    @objc func potassiumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.potassium = 0;return}
        self.potassium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //51
    @objc func lithiumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.lithium = 0;return}
        self.lithium = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //52
    @objc func ammoniaDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.ammonia = 0;return}
        self.ammonia = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //53
    @objc func aluminumDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.aluminum = 0;return}
        self.aluminum = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //54
    @objc func boronDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.boron = 0;return}
        self.boron = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //55
    @objc func bromineDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.bromine = 0;return}
        self.bromine = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //56
    @objc func phosphateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.phosphate = 0;return}
        self.phosphate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //57
    @objc func nitrateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.nitrate = 0;return}
        self.nitrate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //58
    @objc func florideDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.floride = 0;return}
        self.floride = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    //59
    @objc func carbonateDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.carbonate = 0;return}
        self.carbonate = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
    }
    
    private func getToolbar() -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
    }
    
    @IBAction func goBack(_ sender: Any) {
        //        self.present(ScannerViewController(clientId,userId,"analysis"), animated: true, completion: nil)
    }
    
    
    @IBAction func save(_ sender: Any) {
        
        SVProgressHUD.show()
        final_comment_value = commentsTF.text!
        
        let EntryUrl = "\(baseURL)analysis?save_analysis_value=1&machine_id=\(machineId)&user_id=\(userId)&treatpoint_id=\(treatpointId)&analysis_type=\(analysisId)&analysis_date=\(dateValue)&comments=\(final_comment_value)&ph_factor=\(ph)&iron_total=\(ironTotal)&iron_soluble=\(ironSoluble)&alkalinity=\(alkalinity)&total_hardness=\(totalHardness)&oxygen=\(oxygen)&hydrogen_sulfide=\(hydrogenSulfide)&phosophonate=\(phosophonate)&barium=\(barium)&strontium=\(strontium)&total_solids=\(totalSolids)&sample_volume=\(sampleVolume)&hydrocarbon_compounds=\(hydrocarbonCompounds)&acid_soluble=\(acidSoluble)&acid_insoluble=\(acidInsoluble)&formic_acid=\(formicAcid)&acetic_acid=\(aceticAcid)&propionic_acid=\(propionicAcid)&butyric_acid=\(butyricAcid)&valeric_acid=\(valericAcid)&manganese=\(manganese)&calcium=\(calcium)&magnesium=\(magnesium)&silica=\(silica)&chlorides=\(chlorides)&conductivity=\(conductivity)&cid=\(cid)&coupon=\(coupon)&lpr=\(lpr)&er=\(er)&microcorr=\(microcorr)&potentiadyne=\(potentiadyne)&amine_residual=\(amineResidual)&srb=\(srb)&apb=\(apb)&atp=\(atp)&oil_in_water=\(oil)&turbidity=\(turbidity)&carbon_dioxide=\(carbonDioxide)&sulfate=\(sulfate)&zinc=\(zinc)&lead=\(lead)&specific_gravity=\(specificGravity)&links=\(links)&bicarbonate=\(bicarbonate)&days_incubated=\(days)&gab=\(gab)&sodium=\(sodium)&potassium=\(potassium)&lithium=\(lithium)&ammonia=\(ammonia)&aluminum=\(aluminum)&boron=\(boron)&bromine=\(bromine)&phosphate=\(phosphate)&nitrate=\(nitrate)&floride=\(floride)&carbonate=\(carbonate)&last_updated_by=\(clientId)"
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        
        Alamofire.request(encodedUrl!).responseJSON { [weak self] response in
            if let json = response.result.value as? [String: Any]{
                guard let strongSelf = self else {return}
                if (json["response"] as! NSString).intValue == 1{
                    
                    SVProgressHUD.dismiss()
                    
                    let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                        guard let strongSelf = self else {return}
                        
                        //------------------ Gaurav Code --------------------
                        if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                            self?.goToHome()
                            
                        }
                        else{
                            
                            if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                counterOfViewController = counterOfViewController + 1

                                switch val {
                                    
                                    
                                case "form_5":
                                    let vc = WellFailureViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                    
                                case "form_6":
                                    let vc = SamplingViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                case "form_7":
                                    let vc = WellTestViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    
                                    break
                                case "form_8":
                                    let vc = DaysOnViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                case "form_9":
                                    let vc = KnowledgeViewController(strongSelf.machineId,strongSelf.treatpointId,strongSelf.clientId, strongSelf.userId,arrViewM: strongSelf.arr_ViewController! )
                                    vc.modalPresentationStyle = .fullScreen
                                    strongSelf.present(vc, animated: true, completion: nil)
                                    break
                                case "form_10":
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                                    vc.client_id = strongSelf.clientId
                                    vc.user_id = strongSelf.userId
                                    vc.arr_ViewController = strongSelf.arr_ViewController!
                                    strongSelf.present(vc, animated: true, completion: nil)

                                    break
                                default:
                                    break
                                }//Switch
                            }// If Let
                        }//else
                        //------------------ End --------------------
                        
                        
                        
                    }))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
                else {
                    SVProgressHUD.dismiss()
                    
                    let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    strongSelf.present(alert, animated: true, completion: nil)
                }
            }
            else{
                SVProgressHUD.dismiss()
                
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
    
}

extension AnalysisViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == analysisList || textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}
