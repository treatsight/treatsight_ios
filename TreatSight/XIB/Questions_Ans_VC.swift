//
//  Questions_Ans_VC.swift
//  TreatSight
//
//  Created by KUMAR GAURAV on 16/08/20.
//  Copyright © 2020 Expert Cabin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
import SVProgressHUD



class Questions_Ans_VC: UIViewController {

    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?
    var machineValue: String
    var client_id: String = ""
    var user_id: String = ""
    var treatpointId: String = ""
    var productId: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getQuestions()
    }


    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String) {
        self.client_id = client_id
        self.user_id = user
        self.treatpointId = tpId
        self.machineValue = machineId
//        self.arr_ViewController = arrViewM
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func getQuestions(){
//        http://api.treatsight.com/api/ClientQuestions?user_id=8774ae79-91fb-4bcf-9db3-048f2bb0e693&client_id=ff32287f-987b-49ed-94c3-8b58297d404c
        
//        var baseURL: String = "http://api.treatsight.com/api/"
        SVProgressHUD.show()
        Alamofire.request("\(baseURL)ClientQuestions?user_id=\(user_id)&client_id=\(client_id)").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
  
                    SVProgressHUD.dismiss()
            
                    
                }
            }
        }
    }

}
