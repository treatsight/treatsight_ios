//
//  FormViewController.swift
//  TreatPoint
//
//  Created by rodrigo camparo on 12/2/19.
//  Copyright © 2019 Rodrigo Camparo. All rights reserved.
//

import Foundation
import UIKit
import SimpleCheckbox
import iOSDropDown
import Alamofire
import SkyFloatingLabelTextField
import MBProgressHUD
import SVProgressHUD
var strComment:String = ""

var progressHUD = MBProgressHUD()

func removeValueFromNsUserDefaults()
{
    UserDefaults.standard.removeObject(forKey: "user_name")
    UserDefaults.standard.removeObject(forKey: "password")
    UserDefaults.standard.removeObject(forKey: "isRemembered")

}
struct Repository {
    let id: Int
}
class CTMFormViewController: UIViewController {
    
    
    @IBOutlet weak var cons_rate_height: NSLayoutConstraint!
    @IBOutlet weak var cons_rate_value_height: NSLayoutConstraint!
    @IBOutlet weak var cons_previous_date_height: NSLayoutConstraint!
    @IBOutlet weak var cons_previous_date_value_height: NSLayoutConstraint!
    @IBOutlet weak var cons_inventory_volume_height: NSLayoutConstraint!
    @IBOutlet weak var cons_inventory_volume_value_height: NSLayoutConstraint!

    @IBOutlet weak var ml_PostRate: UITextField!
    
    @IBOutlet weak var ml_PreRate: UITextField!
    
    @IBOutlet weak var deleveryVolume_inch: UITextField!
    
    @IBOutlet weak var capsVal: Checkbox!
    @IBOutlet weak var txtFeild_comment: UITextView!
    @IBOutlet weak var dropdown_comment: DropDown!
    @IBOutlet weak var barCode: UITextField!
    @IBOutlet weak var lbl_formCount: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
   // @IBOutlet weak var treatPointList: DropDown!
  //  @IBOutlet weak var productList: DropDown!
    @IBOutlet weak var deliveryDate: UIDatePicker!
    @IBOutlet weak var deliveryVolume: UITextField!
    @IBOutlet weak var inventoryVolume: UITextField!
    @IBOutlet weak var treatmentVolume: UITextField!
    @IBOutlet weak var invoiceTotal: UITextField!
    @IBOutlet weak var invoiceNum: UITextField!
    @IBOutlet weak var lineNum: UITextField!
    @IBOutlet weak var abCheck: Checkbox!
    @IBOutlet weak var flushingCheck: Checkbox!
    @IBOutlet weak var tbcCheck: Checkbox!
    @IBOutlet weak var rodCheck: Checkbox!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var inventory_inch: UITextField!
    @IBOutlet weak var lbl_rate: UILabel!
    @IBOutlet weak var lbl_previous_date: UILabel!
    @IBOutlet weak var lbl_inventory_volume: UILabel!
    
    @IBOutlet weak var ml20Show: UILabel!
    @IBOutlet weak var targetShow: UILabel!
    @IBOutlet weak var ml20Value: UILabel!
    @IBOutlet weak var targetRateValue: UILabel!
    @IBOutlet weak var treatpointShowLabel: UILabel!
    @IBOutlet weak var productShowLabel: UILabel!
    
    
    
    var arr_ViewController : NSMutableArray? = []
    var currentIndex : Int?

    var machineValue: String
    var client_id: String = ""
    var user_id: String = ""
    var treatpointId: String = ""
    var productId: String = ""
    var dateValue: String? = ""
    var dateCurrent: String = ""
    var gals: Float64 = 0.00
    var inches: Float64 = 0.00
    var targetrateShow:  Float64 = 0.00
    var ml20rateShow:  Float64 = 0.00
    var str_previous_date : String = ""
    
    var deliveryVolumeValue: Float = 0
    var inventoryVolumeValue: Float = 0
    var deliveryVolumeValue_inch: Float = 0
    var inventoryVolumeValue_inch: Float = 0
    var treatmentRateValue: Float = 0
    var invoiceTotalValue: Float = 0
    var invoiceNumValue: Int = 0
    var lineNumValue: Int = 0
    var mlPreValue: Float = 0
    var mlPostValue: Float = 0
    
    var previous_DeliveryValue: CGFloat = 0
    var previous_InventoryValue: CGFloat = 0
    var rate_Value: CGFloat = 0
    var days_difference: Int = 0
    var isPreviousRecord: Bool = false
    
    var abCheckValue: Bool = false
    var tbgCheckValue: Bool = false
    var rodCheckValue: Bool = false
    var flushingCheckValue: Bool = false
    var capsCheckValue: Bool = false

    var products =  Dictionary<String, String>()
    var treatpoints =  Dictionary<String, String>()
    var comments_array =  Dictionary<String, String>()
    var treatpoints_all:Any = [:]
    var comment_value: String = ""
    var final_comment_value: String = ""
    var comment_description: String = ""
    var commentsValue: String = ""
    
    struct TreatPointData {
        var treatpoint_id : String
        var treatpoint_name : String
        var strappings_gals : Float64
        var strappings_inches : Float64
        var treatment_volume : Float64
        var ml_20 : Float64
    }
    var TreatPointValues = [TreatPointData]()
    
    @IBAction func logoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Alert !", message: "Are you sure to logout ?", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            removeValueFromNsUserDefaults()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

            self.present(loginVC, animated:true, completion:nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    @IBAction func homeAction(_ sender: Any) {
        self.goToHome()
    }
    
    func goToHome(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        nextViewController.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

        self.present(nextViewController, animated:true, completion:nil)
    }

    
    
    // MARK: - Initializers Methods
    // MARK: -
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        var strCount : String = String(counterOfViewController)
        let arrayCount : Int = (arr_ViewController?.count)!
//        arrayCount = arrayCount + 
        strCount = strCount + " of " + String(arrayCount) as String
        lbl_formCount.text = strCount
        
        if arrayCount == counterOfViewController {
            saveButton.setTitle(saveBtnTitle_atEnd, for: UIControl.State.normal)
        }
        strComment = ""
        
        txtFeild_comment.layer.borderColor = UIColor.darkGray.cgColor
        txtFeild_comment.layer.borderWidth = 1.0
        
        cons_rate_height.constant = 0
        cons_rate_value_height.constant = 0
    }
    
    init( _ machineId: String,_ tpId: String, _ client_id: String, _ user: String,arrViewM:NSMutableArray) {
        self.client_id = client_id
        self.user_id = user
        self.treatpointId = tpId
        self.machineValue = machineId
        self.arr_ViewController = arrViewM
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.scrollView.frame.height)
        
        
        self.treatpointShowLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.treatpointShowLabel.layer.borderWidth = 1.0
        self.treatpointShowLabel.layer.cornerRadius = 5.0
        
        self.productShowLabel.layer.borderColor = UIColor.lightGray.cgColor
        self.productShowLabel.layer.borderWidth = 1.0
        self.productShowLabel.layer.cornerRadius = 5.0
        
        self.targetRateValue.text = String(self.treatpointId)
        self.saveForm_continous();
        
        
        /*
        treatPointList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.treatpointId = strongSelf.treatpoints[string]!
                        
            strongSelf.saveForm_continous()
            
            // i have added
            let treatpoints = self!.TreatPointValues
            for treatpoint in treatpoints {
                if(strongSelf.treatpointId == treatpoint.treatpoint_id){
                    self!.gals = treatpoint.strappings_gals
                    self!.inches = treatpoint.strappings_inches
                
                    self!.targetrateShow = treatpoint.treatment_volume
                    self!.ml20rateShow = treatpoint.ml_20
                
                    self!.targetRateValue.text = String(self!.targetrateShow)
                    self!.ml20Value.text = String(self!.ml20rateShow)
                }
            }
            
            
            if(self!.str_previous_date != ""){
            var calculation:CGFloat = (self!.previous_InventoryValue + self!.previous_DeliveryValue) - CGFloat(self!.inventoryVolumeValue)
            
                calculation = calculation / CGFloat(self!.date_Differnce(current_date: self!.dateCurrent,previous_date: self!.str_previous_date))
                
                            
            if(calculation > 0){
                    self!.lbl_rate.text = String(format: "%.2f",calculation)
                    self!.cons_rate_value_height.constant = 21
                    self!.cons_rate_height.constant = 21
                }else{
                    self!.cons_rate_value_height.constant = 0
                    self!.cons_rate_height.constant = 0
                }
            }
            self!.inventory_inch.text = String(format: "%.2f", self!.inventoryVolumeValue_inch)
            
           
        }
        
        productList.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.productId = strongSelf.products[string]!
             
          
        }
        */
        saveButton.layer.cornerRadius = saveButton.frame.height / 2
       // treatPointList.delegate = self
       // treatPointList.selectedRowColor = themeColor()
        
       // productList.delegate = self
       // productList.selectedRowColor = themeColor()
        txtFeild_comment.inputAccessoryView = getToolbar()

        
        deliveryVolume.addTarget(self, action: #selector(CTMFormViewController.deliveryVolumeDidChange(_:)), for: UIControl.Event.editingChanged)
        deliveryVolume.inputAccessoryView = getToolbar()
        
        deleveryVolume_inch.addTarget(self, action: #selector(CTMFormViewController.deliveryVolumeDidChange_inch(_:)), for: UIControl.Event.editingChanged)
        deleveryVolume_inch.inputAccessoryView = getToolbar()
        
        inventoryVolume.addTarget(self, action: #selector(CTMFormViewController.inventoryVolumeDidChange(_:)), for: UIControl.Event.editingChanged)
        inventoryVolume.inputAccessoryView = getToolbar()
        
        inventory_inch.addTarget(self, action: #selector(CTMFormViewController.inventoryVolumeDidChange_inch(_:)), for: UIControl.Event.editingChanged)
        inventory_inch.inputAccessoryView = getToolbar()

        
        treatmentVolume.addTarget(self, action: #selector(CTMFormViewController.treatmentRateDidChange(_:)), for: UIControl.Event.editingChanged)
        treatmentVolume.inputAccessoryView = getToolbar()
        
        
        invoiceTotal.addTarget(self, action: #selector(CTMFormViewController.invoiceTotalDidChange(_:)), for: UIControl.Event.editingChanged)
        invoiceTotal.inputAccessoryView = getToolbar()
        invoiceNum.addTarget(self, action: #selector(CTMFormViewController.invoiceNumDidChange(_:)), for: UIControl.Event.editingChanged)
        invoiceNum.inputAccessoryView = getToolbar()
        lineNum.addTarget(self, action: #selector(CTMFormViewController.lineNumDidChange(_:)), for: UIControl.Event.editingChanged)
        lineNum.inputAccessoryView = getToolbar()
        
        ml_PreRate.addTarget(self, action: #selector(CTMFormViewController.ml_PreRate(_:)), for: UIControl.Event.editingChanged)
        ml_PreRate.inputAccessoryView = getToolbar()
        
        ml_PostRate.addTarget(self, action: #selector(CTMFormViewController.ml_PostRate(_:)), for: UIControl.Event.editingChanged)
        ml_PostRate.inputAccessoryView = getToolbar()
        
        abCheck.addTarget(self, action: #selector(abcheckValueChanged(sender:)), for: .valueChanged)
        flushingCheck.addTarget(self, action: #selector(flushingcheckValueChanged(sender:)), for: .valueChanged)
        tbcCheck.addTarget(self, action: #selector(tbccheckValueChanged(sender:)), for: .valueChanged)
        rodCheck.addTarget(self, action: #selector(rodcheckValueChanged(sender:)), for: .valueChanged)
        
        capsVal.addTarget(self, action: #selector(capscheckValueChanged(sender:)), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CTMFormViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CTMFormViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
//        self.dateValue = myString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.dateValue = myString
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        self.dateCurrent = formatter.string(from: deliveryDate.date)
        
        SVProgressHUD.show()
        let ENTERURL = "\(baseURL)ctm?user_id=\(user_id)&client_id=\(client_id)&machineid=\(treatpointId)&get_treatpoints"
        print(ENTERURL)
        Alamofire.request(ENTERURL).responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
//                print( json["Data"].null == NSNull() )    // prints true
                if let val:String = json["response"] as? String{//json["response"]{
                    if (val as NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let treatpoints = aux
                   /* var treatpointsResults: [String] = []
                        print(aux)
                    for treatpoint in treatpoints {
                        let value:String = treatpoint["treatpoint_id"] as! String
                        let name: String = treatpoint["treatpoint_name"] as! String
                        
                        
                        
                        treatpointsResults.append(name)
                        
                        self!.TreatPointValues.append(TreatPointData(
                            treatpoint_id: treatpoint["treatpoint_id"] as! String,
                            treatpoint_name: treatpoint["treatpoint_name"] as! String,
                            strappings_gals: treatpoint["strappings_gals"] as! Float64,
                            strappings_inches: treatpoint["strappings_inches"] as! Float64,
                            treatment_volume: treatpoint["treatment_volume"] as! Float64,
                            ml_20: treatpoint["ml_20"] as! Float64
                        ))
                        
                        strongSelf.treatpoints[treatpoint["treatpoint_name"] as! String] = value
                    }
                    strongSelf.treatPointList.optionArray = treatpointsResults
                    */
                        SVProgressHUD.dismiss()
                        
 //                    strongSelf.treatPointList.optionIds = treatpointsIds
                 //   strongSelf.treatPointList.selectedIndex = 0
                        
                    strongSelf.gals = treatpoints[0]["strappings_gals"] as! Float64
                    strongSelf.inches = treatpoints[0]["strappings_inches"] as! Float64

                    strongSelf.targetrateShow = treatpoints[0]["treatment_volume"] as! Float64
                    strongSelf.ml20rateShow = treatpoints[0]["ml_20"] as! Float64
                        
                    self!.targetRateValue.text = String(strongSelf.targetrateShow)
                    self!.ml20Value.text = String(strongSelf.ml20rateShow)
                        
                    strongSelf.treatpointId = treatpoints[0]["treatpoint_id"] as! String
                    strongSelf.productId = treatpoints[0]["product_id"] as! String
                        strongSelf.treatpointShowLabel.text = " "+String(treatpoints[0]["treatpoint_name"] as! String) + " "
                       
                        strongSelf.productShowLabel.text = " "+String(treatpoints[0]["product_name"] as! String)+" "
                        //strongSelf.treatPointList.text = treatpointsResults[0]
                    strongSelf.saveForm_continous()
                    
                }
                }
                else{
                    print("problem in parsing")
                }
            }
        }
        
        
       /* SVProgressHUD.show()
        Alamofire.request("\(baseURL)product").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    var productsResults: [String] = []
                    for product in products {
                        productsResults.append(product["product_name"]!)
                        strongSelf.products[product["product_name"]!] = product["product_id"]
                    }
                    SVProgressHUD.dismiss()
                    strongSelf.productList.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.productList.selectedIndex = 0
                    strongSelf.productId = products[0]["product_id"]!
                    strongSelf.productList.text = productsResults[0]

                    
                }
            }
        }*/
        
        
        dropdown_comment.didSelect { [weak self](string: String, i: Int, x: Int) in
            guard let strongSelf = self else {return}
            strongSelf.comment_value = strongSelf.comments_array[string]!
            
        }
        dropdown_comment.delegate = self
        dropdown_comment.selectedRowColor = themeColor()

        
        Alamofire.request("\(baseURL)StandardComment").responseJSON {[weak self] response in
            if let json = response.result.value as? [String: Any]{
                if (json["response"] as! NSString).intValue == 1{
                    guard let strongSelf = self else {return}
                    var productsResults: [String] = []
                    let aux = json["data"] as! [[String: Any]]
                    let products = aux as! [[String: String]]
                    //entry_value
                    for product in products {
                        productsResults.append(product["entry_description"]!)
                        strongSelf.comments_array[product["entry_description"]!] = product["entry_value"]
                    }
                    strongSelf.dropdown_comment.optionArray = productsResults
                    //strongSelf.productList.optionIds = productsIds
                    strongSelf.dropdown_comment.selectedIndex = 0
                    strongSelf.comment_value = products[0]["entry_value"]!
                    strongSelf.dropdown_comment.text = productsResults[0]
                }
            }
        }//End Of Comment
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func parseRepository(json: NSDictionary) -> Repository? {
        
        guard let id = json["response"] as? Int else {
            print("Couldn't convert id to an Int")
            return nil
        }
        // TODO: parse the rest of the JSON...
        return Repository(id: id)
    }
        
    @IBAction func replaceClicked(_ sender: Any) {
        strComment = ""
        strComment = dropdown_comment.text!
       txtFeild_comment.text  = strComment
       final_comment_value = comment_value
    }
    
    @IBAction func appendClicked(_ sender: Any) {
        strComment = ""
        strComment = txtFeild_comment.text! + dropdown_comment.text!
        strComment = strComment.trimmingCharacters(in: .whitespaces)
        //print(strComment)
        txtFeild_comment.text  = strComment
        final_comment_value = txtFeild_comment.text! 
    }

    
    @IBAction func dateChanged(_ sender: Any) {
        let a = sender as! UIDatePicker
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: a.date) // string purpose I add here
        self.dateValue = myString
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        self.dateCurrent = formatter.string(from: a.date)
        
        if(self.str_previous_date != ""){
        var calculation:CGFloat = (self.previous_InventoryValue + self.previous_DeliveryValue) - CGFloat(self.inventoryVolumeValue)
        
        calculation = calculation / CGFloat(date_Differnce(current_date: self.dateCurrent, previous_date: self.str_previous_date))
            
                if(calculation > 0){
                    lbl_rate.text = String(format: "%.2f",calculation)
                    
                    cons_rate_value_height.constant = 21
                    cons_rate_height.constant = 21
                }else{
                    cons_rate_value_height.constant = 0
                    cons_rate_height.constant = 0
                }
        }
        
        
        
        //print("date is :")
        //print(CGFloat(date_Differnce(current_date: self.dateCurrent,previous_date: self.str_previous_date)))
        
        if (self.productId != "" && self.dateValue != "" && self.treatpointId != "" ) {
            self.saveButton.isEnabled = true
            self.saveButton.alpha = 1
        } else {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.5
        }
    }
    
    
   
    
    @objc func deliveryVolumeDidChange(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.deliveryVolumeValue = 0; return}
        self.deliveryVolumeValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
        self.deliveryVolumeValue_inch = self.deliveryVolumeValue*Float((inches/gals))
       self.deleveryVolume_inch.text = String(format: "%.2f", self.deliveryVolumeValue_inch)
        print(self.deliveryVolumeValue_inch)
        print(self.deleveryVolume_inch.text as Any)

    }
    
 
    
    @objc func deliveryVolumeDidChange_inch(_ textField: UITextField) {
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.deliveryVolumeValue_inch = 0; return}
        self.deliveryVolumeValue_inch = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        
        self.deliveryVolumeValue = self.deliveryVolumeValue_inch*Float((gals/inches))
        self.deliveryVolume.text = String(format: "%.2f", self.deliveryVolumeValue)
        print(self.deliveryVolumeValue)
        print(self.deliveryVolume.text as Any)

    }
    
    @objc func inventoryVolumeDidChange(_ textField: UITextField) {
        
        if !(textField.text! == "" ){

            guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.inventoryVolumeValue = 0; return}
            self.inventoryVolumeValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
            
            self.inventoryVolumeValue_inch = self.inventoryVolumeValue*Float((inches/gals))
            if(self.str_previous_date != ""){
            var calculation:CGFloat = (self.previous_InventoryValue + self.previous_DeliveryValue) - CGFloat(self.inventoryVolumeValue)
            
            calculation = calculation / CGFloat(date_Differnce(current_date: self.dateCurrent,previous_date: self.str_previous_date))
                
                            
            if(calculation > 0){
                lbl_rate.text = String(format: "%.2f",calculation)
                    cons_rate_value_height.constant = 21
                    cons_rate_height.constant = 21
                }else{
                    cons_rate_value_height.constant = 0
                    cons_rate_height.constant = 0
                }
            }
            self.inventory_inch.text = String(format: "%.2f", self.inventoryVolumeValue_inch)
            //print(self.inventoryVolumeValue_inch)
            
            //print(self.inventory_inch.text as Any)
        }else{
            cons_rate_value_height.constant = 0
            cons_rate_height.constant = 0
            self.inventory_inch.text = String(format: "%.2f", 0.0)
        }
        
    }
    
    @objc func inventoryVolumeDidChange_inch(_ textField: UITextField) {
        if !(textField.text! == ""){
        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.inventoryVolumeValue_inch = 0; return}
        self.inventoryVolumeValue_inch = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
            
            
            self.inventoryVolumeValue = self.inventoryVolumeValue_inch*Float((gals/inches))
            self.inventoryVolume.text = String(format: "%.2f", self.inventoryVolumeValue)
            
            
            if(self.str_previous_date != ""){
                
                var calculation:CGFloat = (self.previous_InventoryValue + self.previous_DeliveryValue) -                CGFloat(self.inventoryVolumeValue)
        
                calculation = calculation / CGFloat(date_Differnce(current_date: self.dateCurrent,previous_date: self.str_previous_date))
        
                if(calculation > 0){
                    lbl_rate.text = String(format: "%.2f",calculation)
                cons_rate_value_height.constant = 21
                        cons_rate_height.constant = 21
                    }else{
                        cons_rate_value_height.constant = 0
                        cons_rate_height.constant = 0
                    }
            }
                
               // print(self.inventoryVolumeValue)
               // print(self.inventoryVolume.text as Any)
            }else{
                cons_rate_value_height.constant = 0
                cons_rate_height.constant = 0
            self.inventoryVolume.text = String(format: "%.2f", 0.0)
            }
    }

    func date_Differnce(current_date : String, previous_date : String)->Int{
        let dateFormatter = DateFormatter()
        var diff : Int?
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        // "2019-09-11 15:08:41"
        //let date  = Date()
        if dateFormatter.date(from: previous_date) != nil {
            let current = dateFormatter.date(from: current_date)
            let previous = dateFormatter.date(from: previous_date)//(from: previous_date)
            let interval = Calendar.current.dateComponents([.day], from: previous!, to: current!)
            diff = interval.day
        } else {
            diff = 0;
        }
        return diff!
    }
    
    @objc func treatmentRateDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.treatmentRateValue = 0; return}
        self.treatmentRateValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        }
    }
    
    @objc func invoiceTotalDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){

        guard Float(textField.text!.replacingOccurrences(of: ",", with: ".")) != nil else {self.invoiceTotalValue = 0; return}
        self.invoiceTotalValue = Float(textField.text!.replacingOccurrences(of: ",", with: "."))!
        }
    }
    
    @objc func invoiceNumDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){
        self.invoiceNumValue = Int(textField.text!)!
        }
    }
    
    @objc func lineNumDidChange(_ textField: UITextField) {
        if !(textField.text! == ""){
        self.lineNumValue = Int(textField.text!)!
        }
    }
    
    @objc func ml_PreRate(_ textField: UITextField) {
        if !(textField.text! == ""){
        self.mlPreValue = Float(textField.text!)!
        }
    }
    
    @objc func ml_PostRate(_ textField: UITextField) {
        self.mlPostValue = Float(textField.text!)!
    }
    
    @objc func abcheckValueChanged(sender: Checkbox) {
        self.abCheckValue = sender.isChecked
    }
    
    @objc func flushingcheckValueChanged(sender: Checkbox) {
        self.flushingCheckValue = sender.isChecked
    }
    
    @objc func tbccheckValueChanged(sender: Checkbox) {
        self.tbgCheckValue = sender.isChecked
    }
    
    @objc func rodcheckValueChanged(sender: Checkbox) {
        self.rodCheckValue = sender.isChecked
    }
    
    @objc func capscheckValueChanged(sender: Checkbox) {
        self.capsCheckValue = sender.isChecked
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
            

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getValueFromTextFeild(){
        
    }
    
    
    @IBAction func saveForm(_ sender: Any) {
//        let final_comment_value : String = txtFeild_comment.text!
        SVProgressHUD.show()

        let EntryUrl = "\(baseURL)CTM?product_id=\(productId)&treatpoint_id=\(treatpointId)&delivery_date=\(dateValue!)&last_updated_by=\(client_id)&comments=\(final_comment_value)&delivery_volume=\(deliveryVolumeValue)&delivery_volume_level=\(deliveryVolumeValue_inch)&inventory_volume=\(inventoryVolumeValue)&inventory_volume_level=\(inventoryVolumeValue_inch)&treatment_rate=\(treatmentRateValue)&invoice_total=\(invoiceTotalValue)&invoice_num=\(invoiceNumValue)&line_num=\(lineNumValue)&pre_rate=\(mlPreValue)&post_rate=\(mlPostValue)&ab1960=\(abCheckValue)&tbg_rotator=\(tbgCheckValue)&rod_rotator=\(rodCheckValue)&flushing=\(flushingCheckValue)&caps=\(capsCheckValue)&save_ctm_form_value=1&machine_id=\(machineValue)&user_id=\(user_id)"
        //print(EntryUrl)
        
        let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

//        var postParameters:[String: Any] = [:]
        
        
        Alamofire.request(encodedUrl!, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers:nil).responseJSON {
            response in
            if response.result.isSuccess {
                SVProgressHUD.dismiss()

                print("SuccessFully Added")
                if let json = response.result.value as? [String: Any]{
                    if (json["response"] as! NSString).intValue == 1{
                        SVProgressHUD.dismiss()
                        let alert = UIAlertController(title: "Success", message: "Form successfully submited", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] action in
                            guard let strongSelf = self else {return}
                            //------------------ Gaurav Code --------------------
                            if self!.saveButton.titleLabel?.text == saveBtnTitle_atEnd{
                                self?.goToHome()
                            }
                            else{
                                
                                if let val: String = (self?.arr_ViewController?[counterOfViewController] as! String){
                                    counterOfViewController = counterOfViewController + 1


                                    switch val {
                                    case "form_2":
                                        let vc = BatchTruckViewController(self!.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController!)
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        
                                        break
                                    case "form_3":
                                        
                                        let vc = MonitoringPlanViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController!)
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        
                                        break
                                    case "form_4":
                                        let vc = MonitoringPlanViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController!)
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        
                                        break
                                    case "form_5":
                                        let vc = WellFailureViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController! )
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        break
                                        
                                    case "form_6":
                                        let vc = SamplingViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController! )
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        break
                                    case "form_7":
                                        let vc = WellTestViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController! )
                                        vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        
                                        break
                                    case "form_8":
                                       let vc = DaysOnViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController! )
                                       vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        break
                                    case "form_9":
                                        let vc = KnowledgeViewController(strongSelf.machineValue,strongSelf.treatpointId,strongSelf.client_id, strongSelf.user_id,arrViewM: strongSelf.arr_ViewController! )
                                       vc.modalPresentationStyle = .fullScreen
                                        strongSelf.present(vc, animated: true, completion: nil)
                                        break
                                    case "form_10":
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let vc = storyBoard.instantiateViewController(withIdentifier: "Questions_VC") as! Questions_VC
                                        vc.client_id = strongSelf.client_id
                                        vc.user_id = strongSelf.user_id
                                        vc.arr_ViewController = strongSelf.arr_ViewController!
                                        strongSelf.present(vc, animated: true, completion: nil)

                                        break
                                    default:
                                        break
                                    }//Switch
                                }// If Let
                            }//else
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        SVProgressHUD.dismiss()
                        
                        let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
            }else{
                SVProgressHUD.dismiss()

                print("Error \(String(describing: response.result.error))")
                
            }
    }
    }
    
    
    


    func saveForm_continous() {
            SVProgressHUD.show()
            
            let EntryUrl = "\(baseURL)PreviousCTM?treatpoint=\(treatpointId)"
            print(EntryUrl)
            let encodedUrl = EntryUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            Alamofire.request(encodedUrl!, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers:nil).responseJSON {
                response in
                if response.result.isSuccess {
                    SVProgressHUD.dismiss()
                    print("SuccessFully Added")
                    if let json = response.result.value as? [String: Any]{
                        if (json["response"] as! NSString).intValue == 1{
                            self.isPreviousRecord = true
                            self.ispreviousRecord_willDisplay(val: true)
                            SVProgressHUD.dismiss()
                            let dic_previous_record : NSDictionary = json["data"] as! NSDictionary
                            self.previous_InventoryValue = dic_previous_record.value(forKey: "inventory_volume") as! CGFloat
                            self.previous_DeliveryValue = dic_previous_record.value(forKey: "delivery_volume") as! CGFloat
                            self.str_previous_date  = dic_previous_record.value(forKey: "delivery_date") as! String
                            /* ------- */
                            
                            /* ------- */
                         
                            self.lbl_inventory_volume.text = String(format: "%.2f", self.previous_InventoryValue)
                            
                            var str : String = dic_previous_record.value(forKey: "delivery_date") as! String
                            
                            str = str.components(separatedBy: "T")[0]
                            
                            let dd : String = str.components(separatedBy: "-")[2]
                            let mm : String = str.components(separatedBy: "-")[1]
                            let yy : String = str.components(separatedBy: "-")[0]
                            
                            self.lbl_previous_date.text = dd+"-"+mm+"-"+yy
                            
                            
                            //self.lbl_previous_date.text = self.str_previous_date

 
                        } else {
                            self.isPreviousRecord = false
                            self.ispreviousRecord_willDisplay(val: false)
                            
                            SVProgressHUD.dismiss()
                           /* let alert = UIAlertController(title: "Error", message: "An error ocurred", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)*/
                        }
                    }
                    
                }else{
                    SVProgressHUD.dismiss()
                    print("Error \(String(describing: response.result.error))")
                }
            }
        }
    
    func ispreviousRecord_willDisplay(val : Bool){
        if val{

            self.cons_previous_date_height.constant = 21
            self.cons_previous_date_value_height.constant = 21
            self.cons_inventory_volume_height.constant = 21
            self.cons_inventory_volume_value_height.constant = 21
        }
        else{
            self.cons_rate_value_height.constant = 0
            self.cons_rate_height.constant = 0
            self.cons_previous_date_height.constant = 0
            self.cons_previous_date_value_height.constant = 0
            self.cons_inventory_volume_height.constant = 0
            self.cons_inventory_volume_value_height.constant = 0
        }
    }
    
    func getToolbar() -> UIToolbar {

        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:320, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(CTMFormViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        return doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
    
}

extension CTMFormViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == dropdown_comment){
            return false
        } else {
            return true
        }
    }
}


